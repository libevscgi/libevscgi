# Find Judy (a hash-tree dictionary library)

set(Judy_EXTRA_PREFIXES /usr/local /opt/local "$ENV{HOME}")
foreach(prefix ${Judy_EXTRA_PREFIXES})
  list(APPEND Judy_INCLUDE_PATHS "${prefix}/include")
  list(APPEND Judy_LIB_PATHS "${prefix}/lib")
endforeach()

find_path(JUDY_INCLUDE_DIR Judy.h PATHS ${Judy_INCLUDE_PATHS})
find_library(JUDY_LIBRARY Judy PATHS ${Judy_LIB_PATHS})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Judy DEFAULT_MSG JUDY_INCLUDE_DIR JUDY_LIBRARY)
