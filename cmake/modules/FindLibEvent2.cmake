# Find LibEvent2 (a cross event library)

set(LibEvent2_EXTRA_PREFIXES /usr/local /opt/local "$ENV{HOME}")
foreach(prefix ${LibEvent2_EXTRA_PREFIXES})
	list(APPEND LibEvent2_INCLUDE_PATHS "${prefix}/include")
	list(APPEND LibEvent2_LIB_PATHS "${prefix}/lib")
endforeach()

find_path(LIBEVENT2_INCLUDE_DIR event2/event.h PATHS ${LibEvent2_INCLUDE_PATHS})
find_library(LIBEVENT2_LIB NAMES event PATHS ${LibEvent2_LIB_PATHS})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LibEvent2 DEFAULT_MSG LIBEVENT2_INCLUDE_DIR LIBEVENT2_LIB)
