/*
 * Copyright (c) 2002-2007 Niels Provos <provos@citi.umich.edu>
 * Copyright (c) 2007-2011 Niels Provos and Nick Mathewson
 * Copyright (c) 2012 Andrew Munkres <amunkres@nyx.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <assert.h>
#include <ctype.h>

#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/bufferevent_compat.h>
#include <event2/listener.h>
#include <event2/http.h> /* For URI and HTML (and maybe headers later on) processing functions only */

#include <Judy.h>

#include "evscgi.h"

#define SCGI_CONNECT_TIMEOUT  45
#define SCGI_WRITE_TIMEOUT  50
#define SCGI_READ_TIMEOUT 50

/* Based on evhttp_request_kind */
enum evscgi_request_kind { EVSCGI_REQUEST, EVSCGI_RESPONSE };

enum evscgi_connection_state
{
  /* Based on evhttp_connection_state */
  EVCON_DISCONNECTED, /**< not currently connected not trying either*/
//   EVCON_CONNECTING, /**< tries to currently connect */ // Makes sense only in client mode
//   EVCON_IDLE,   /**< connection is established */ // Not applicable to SCGI because connections are never reusable
//   EVCON_READING_FIRSTLINE,/**< reading Request-Line (incoming conn) or
//          **< Status-Line (outgoing conn) */ // Not applicable to SCGI because there's really no such thing as "first line".
  EVCON_READING_HEADERS,  /**< reading request/response headers */
  EVCON_READING_BODY, /**< reading request/response body */
//   EVCON_READING_TRAILER,  /**< reading request/response chunked trailer */ // Not applicable to SCGI because requests are never chunked
  EVCON_WRITING   /**< writing request/response headers/body */
};

// Request processing states originally from http-internal.h:
enum message_read_status {
  ALL_DATA_READ = 1,
  MORE_DATA_EXPECTED = 0,
  DATA_CORRUPTED = -1,
  REQUEST_CANCELED = -2,
  DATA_TOO_LONG = -3
};

// Connection error statuses originally from http-internal.h:
enum evscgi_connection_error {
  EVCON_SCGI_TIMEOUT,
  EVCON_SCGI_EOF,
  EVCON_SCGI_INVALID_HEADER,
  EVCON_SCGI_BUFFER_ERROR,
  EVCON_SCGI_REQUEST_CANCEL
};

struct evscgi_uri;
struct evscgi_connection;

/* Based on evhttp_request */
struct evscgi_request {
  TAILQ_ENTRY(evscgi_request) next;

  /* the connection object that this request belongs to */
  struct evscgi_connection *evcon;
  int flags;
/** The request obj owns the evscgi connection and needs to free it */
#define EVSCGI_REQ_OWN_CONNECTION 0x0001
/** Request was made via a proxy */
#define EVSCGI_PROXY_REQUEST    0x0002
/** The request object is owned by the user; the user must free it */
#define EVSCGI_USER_OWNED   0x0004
/** The request will be used again upstack; freeing must be deferred */
#define EVSCGI_REQ_DEFER_FREE   0x0008
/** The request should be freed upstack */
#define EVSCGI_REQ_NEEDS_FREE   0x0010

  struct evkeyvalq *input_headers;
  struct evkeyvalq *output_headers;

  // TODO: maybe don't need this for SCGI
//   /* address of the remote host and the port connection came from */
//   char *remote_host;
//   ev_uint16_t remote_port;

  /* cache of the hostname for evscgi_request_get_host */
  char *host_cache;

  enum evscgi_request_kind kind;
  enum evscgi_cmd_type type;

  size_t headers_size;
  size_t headers_declared_size;
  bool getting_hdr_value; /* indicates whether we're currently reading the key or the value */
  // NOTE: Shouldn't these next two be ev_uint64_t? Because that's what max_body_size in evscgi_connection is.
  ev_uint64_t body_size;
  ev_uint64_t body_declared_size;
  bool have_body_declared_size; /* needed because body_declared_size can be 0 */

  bool found_scgi_hdr;
  
  char *uri;      /* uri after SCGI request was parsed */
  struct evhttp_uri *uri_elems; /* uri elements */

  // NOTE: We'll still track the HTTP version number even in SCGI, because the SERVER_REQUEST SCGI header says what version number the HTTP client sent, and this lets us know whether it's safe to use "Transfer-Encoding: chunked" or not.
  char major;     /* HTTP Major number */
  char minor;     /* HTTP Minor number */

  int response_code;    /* HTTP Response code */
  char *response_code_line; /* Readable response */

  struct evbuffer *input_buffer;  /* read data */
//   ev_int64_t ntoread;
  unsigned chunked:1,   /* a chunked request */
      userdone:1;     /* the user has sent all data */

  struct evbuffer *output_buffer; /* outgoing post or data */

  /* Internal callback and its argument */
  // TODO: Maybe remove this, because it typically is always set to evscgi_handle_request (with the argument set to the server instance for this request), and we've revised the public callback API so we should never need to use a user-supplied callback for this one.
//   void (*cb)(struct evscgi_request *, void *);
//   void *cb_arg;

  /*
   * Chunked data callback - call for each completed chunk if
   * specified.  If not specified, all the data is delivered via
   * the regular callback.
   */
  void (*chunk_cb)(struct evscgi_request *, void *);
  void (*chunksdone_cb)(struct evscgi_request *, void *); // Called after the last chunk is completed.
  void *chunk_cb_arg;
  void (*header_cb)(struct evscgi_request *, void *, const char *, const char *); // Called for each completed header.
  void (*headersdone_cb)(struct evscgi_request *, void *); // Called after the last header is completed.
  void *header_cb_arg;
};

// NOTE: On second thought, is there any real reason why we can't just use evhttp_uri and its functions? Let's try that.
// struct evscgi_uri
// {
//   /* Based on evhttp_uri */
//   unsigned flags;
// //   char *scheme; /* scheme; e.g http, ftp etc */
// //   char *userinfo; /* userinfo (typically username:pass), or NULL */
// //   char *host; /* hostname, IP address, or NULL */
// //   int port; /* port, or zero */
//   char *path; /* path, or "". */
//   char *query; /* query, or NULL */
//   char *fragment; /* fragment or NULL */
// };

#define REQ_VERSION_BEFORE(req, major_v, minor_v)     \
  ((req)->major < (major_v) ||          \
      ((req)->major == (major_v) && (req)->minor < (minor_v)))

#define REQ_VERSION_ATLEAST(req, major_v, minor_v)      \
  ((req)->major > (major_v) ||          \
      ((req)->major == (major_v) && (req)->minor >= (minor_v)))

// NOTE: We can't use this deferred-callback mechanism from outside of libevent (the functions are internal-only). The event_active function seems to be the way to get the same effect, but I believe we don't really need it in libevscgi.

// // Deferred callback structure originally from defer-internal.h:
// struct deferred_cb;
// 
// typedef void (*deferred_cb_fn)(struct deferred_cb *, void *);
// 
// /** A deferred_cb is a callback that can be scheduled to run as part of
//  * an event_base's event_loop, rather than running immediately. */
// struct deferred_cb {
//   /** Links to the adjacent active (pending) deferred_cb objects. */
//   TAILQ_ENTRY (deferred_cb) cb_next;
//   /** True iff this deferred_cb is pending in an event_base. */
//   unsigned queued : 1;
//   /** The function to execute when the callback runs. */
//   deferred_cb_fn cb;
//   /** The function's second argument. */
//   void *arg;
// };

// List node for connection-listeners originally from http-internal.h:
/* each bound socket is stored in one of these */
struct evscgi_bound_socket {
  TAILQ_ENTRY(evscgi_bound_socket) next;

  struct evconnlistener *listener;
};

/* A client or server connection. */
struct evscgi_connection
{
  /* Based on evhttp_connection */
  /* we use this tailq only if this connection was created for an scgi
   * server */
  TAILQ_ENTRY(evscgi_connection) next;

  evutil_socket_t fd;
  struct bufferevent *bufev;

  struct event retry_ev;    /* for retrying connects */

  // TODO: use Unix domain sockets instead
//   char *bind_address;   /* address to use for binding the src */
//   u_short bind_port;    /* local port for binding the src */
// 
//   char *address;      /* address to connect to */
//   u_short port;
  char *path;      /* Unix domain socket to connect to */

  size_t max_headers_size;
  ev_uint64_t max_body_size;

  int flags;
#define EVSCGI_CON_INCOMING 0x0001  /* only one request on it ever */
#define EVSCGI_CON_OUTGOING 0x0002  /* multiple requests possible */
#define EVSCGI_CON_CLOSEDETECT  0x0004  /* detecting if persistent close */

  int timeout;      /* timeout in seconds for events */
  int retry_cnt;      /* retry count */
  int retry_max;      /* maximum number of retries */

  enum evscgi_connection_state state;

  /* for server connections, the scgi server they are connected with */
  struct evscgi *scgi_server;

  TAILQ_HEAD(evcon_requestq, evscgi_request) requests;

  void (*cb)(struct evscgi_connection *, void *);
  void *cb_arg;

  void (*closecb)(struct evscgi_connection *, void *);
  void *closecb_arg;

//   struct deferred_cb read_more_deferred_cb;

  struct event_base *base;
  struct evdns_base *dns_base;
};

TAILQ_HEAD(evconq, evscgi_connection);

struct evscgi
{
  /* Based on struct evhttp */

  /* All listeners for this host */
  TAILQ_HEAD(boundq, evscgi_bound_socket) sockets;

  TAILQ_HEAD(scgicbq, evscgi_cb) callbacks;
  
  /* All live connections on this host. */
  struct evconq connections;

  int timeout;

  size_t default_max_headers_size;
  ev_uint64_t default_max_body_size;

  /* Bitmask of all REQUEST_METHODs that we accept and pass to user
   * callbacks. */
  ev_uint16_t allowed_methods;

  /* Mode for at what stage of a request to first notify the program of the request. */
  enum evscgi_notify_mode notify_on;
  
  /* Fallback callback if all the other callbacks for this connection
     don't match. */
  void (*gencb)(struct evscgi_request *req, void *);
  void *gencbarg;
  
  struct event_base *base;
  
  /* String-keyed dictionaries: */
  Pvoid_t hdrparsers_by_name; /* the names of all SCGI headers we need to parse */
  Pvoid_t methods_by_name; /* the names of all recognized HTTP methods */
};

/* A callback for an http server */
struct evscgi_cb
{
  /* Based on evhttp_cb */
  TAILQ_ENTRY(evscgi_cb) next;

  char *what;

  void (*cb)(struct evscgi_request *req, void *);
  void *cbarg;
};

// Declare header-parser functions here:
static bool parsehdr_SCGI(struct evscgi_request *req, const char *value);
static bool parsehdr_REQUEST_URI(struct evscgi_request *req, const char *value);
static bool parsehdr_REQUEST_METHOD(struct evscgi_request *req, const char *value);
static bool parsehdr_SERVER_PROTOCOL(struct evscgi_request *req, const char *value);

static const char * const hdrparser_names[] = {"SCGI", "REQUEST_URI", "REQUEST_METHOD", "SERVER_PROTOCOL"};
static bool (* const hdrparser[])(struct evscgi_request *req, const char *value) = {parsehdr_SCGI, parsehdr_REQUEST_URI, parsehdr_REQUEST_METHOD, parsehdr_SERVER_PROTOCOL};

static const char * const method_names[] = {"GET", "POST", "HEAD", "PUT", "DELETE", "OPTIONS", "TRACE", "PATCH"};

// Function declarations for internal-only (non-API) functions:
static evutil_socket_t bind_unix_socket(const char *pathname, int reuse);
static struct evscgi_connection *evscgi_get_request_connection(struct evscgi* scgi, evutil_socket_t fd, struct sockaddr *sa, ev_socklen_t salen);
static int evscgi_associate_new_request_with_connection(struct evscgi_connection *evcon);
static void evscgi_start_read(struct evscgi_connection *evcon);
static void evscgi_get_request(struct evscgi *scgi, evutil_socket_t fd, struct sockaddr *sa, ev_socklen_t salen);
static void evscgi_handle_request(struct evscgi_request *req, void *arg);
static struct evscgi_cb *evscgi_dispatch_callback(struct scgicbq *callbacks, struct evscgi_request *req);
static int evhttp_decode_uri_internal(const char *uri, size_t length, char *ret, int decode_plus_ctl);
static void accept_socket_cb(struct evconnlistener *listener, evutil_socket_t nfd, struct sockaddr *peer_sa, int peer_socklen, void *arg);
static void evscgi_read_cb(struct bufferevent *bufev, void *arg);
// static void evscgi_deferred_read_cb(struct deferred_cb *cb, void *data);
static void evscgi_write_cb(struct bufferevent *bufev, void *arg);
static void evscgi_error_cb(struct bufferevent *bufev, short what, void *arg);
static bool evscgi_connected(const struct evscgi_connection *evcon);
static void evscgi_connection_done(struct evscgi_connection *evcon);
static void evscgi_connection_fail(struct evscgi_connection *evcon, enum evscgi_connection_error error);
static int evscgi_connection_incoming_fail(struct evscgi_request *req, enum evscgi_connection_error error);
static void evscgi_read_header(struct evscgi_connection *evcon, struct evscgi_request *req);
static enum message_read_status evscgi_parse_headers(struct evscgi_request *req, struct evbuffer* buffer);
static size_t validate_decimal_string(const char *decimal, size_t decimal_len);
static void evscgi_get_body(struct evscgi_connection *evcon, struct evscgi_request *req);
static void evscgi_read_body(struct evscgi_connection *evcon, struct evscgi_request *req);
void evscgi_response_code(struct evscgi_request *req, int code, const char *reason);
static inline void evscgi_send(struct evscgi_request *req, struct evbuffer *databuf);
static void evscgi_send_done(struct evscgi_connection *evcon, void *arg);
static void evscgi_write_buffer(struct evscgi_connection *evcon, void (*cb)(struct evscgi_connection *, void *), void *arg);
static void evscgi_make_header(struct evscgi_connection *evcon, struct evscgi_request *req);
static void evscgi_make_header_response(struct evscgi_connection *evcon, struct evscgi_request *req);
static void evscgi_maybe_add_content_length_header(struct evkeyvalq *headers, size_t content_length);
static int evscgi_response_needs_body(struct evscgi_request *req);
static const char *evscgi_response_phrase_internal(int code);
static void evscgi_send_page(struct evscgi_request *req, struct evbuffer *databuf);
static struct evscgi_request *evscgi_request_new();
static void evscgi_request_free(struct evscgi_request *req);

struct evscgi *evscgi_new_object(void)
{
  /* Based on evhttp_new_object */
  struct evscgi *scgi = NULL;
  Word_t i;
  PWord_t dict_value_ptr;
  
  if ((scgi = calloc(1, sizeof(struct evscgi))) == NULL)
  {
    /* event_warn("%s: calloc", __func__); */
    return (NULL);
  }
  
  scgi->timeout = -1;
  evscgi_set_max_headers_size(scgi, EV_SIZE_MAX);
  evscgi_set_max_body_size(scgi, EV_SIZE_MAX);
  // TODO: implement filtering requests by method
  evscgi_set_allowed_methods(scgi,
      EVSCGI_REQ_GET |
      EVSCGI_REQ_POST |
      EVSCGI_REQ_HEAD |
      EVSCGI_REQ_PUT |
      EVSCGI_REQ_DELETE);
  
  TAILQ_INIT(&scgi->sockets);
  TAILQ_INIT(&scgi->callbacks);
  TAILQ_INIT(&scgi->connections);
  
  scgi->notify_on = EVSCGI_NOTIFY_RESPONSE_NEEDED;
  // NOTE: We default to EVSCGI_NOTIFY_RESPONSE_NEEDED mode for backwards compatibility with evhttp, where this was the only supported behavior.
  
  scgi->hdrparsers_by_name = (PWord_t) NULL;
  for (i = 0; i < (sizeof(hdrparser_names) / sizeof(char *)); ++i)
  {
    JSLI(dict_value_ptr, scgi->hdrparsers_by_name, hdrparser_names[i]);
    *dict_value_ptr = i;
  }
  
  scgi->methods_by_name = (PWord_t) NULL;
  for (i = 0; i < (sizeof(method_names) / sizeof(char *)); ++i)
  {
    JSLI(dict_value_ptr, scgi->methods_by_name, method_names[i]);
    *dict_value_ptr = (Word_t) (1 << i);
  }
  
  return (scgi);
}

struct evscgi *evscgi_new(struct event_base *base)
{
  /* Based on evhttp_new */
  struct evscgi *scgi = NULL;
  
  scgi = evscgi_new_object();
  if (scgi == NULL)
    return (NULL);
  scgi->base = base;
  
  return (scgi);
}

int evscgi_bind_unix_socket(struct evscgi *scgi, const char *pathname)
{
  // Based on evhttp_bind_socket
  struct evscgi_bound_socket *bound = evscgi_bind_unix_socket_with_handle(scgi, pathname);
  if (bound == NULL)
    return (-1);
  return (0);
}

struct evscgi_bound_socket *evscgi_bind_unix_socket_with_handle(struct evscgi *scgi, const char *pathname)
{
  // Based on evhttp_bind_socket_with_handle
  evutil_socket_t fd;
  struct evscgi_bound_socket *bound;

  if ((fd = bind_unix_socket(pathname, 1 /*reuse*/)) == -1)
    return (NULL);

  if (listen(fd, 128) == -1) {
//     event_sock_warn(fd, "%s: listen", __func__);
    evutil_closesocket(fd);
    return (NULL);
  }

  bound = evscgi_accept_socket_with_handle(scgi, fd);

  if (bound != NULL) {
//     event_debug(("Bound to port %d - Awaiting connections ... ",
//       port));
    return (bound);
  }

  return (NULL);
}

int evscgi_accept_socket(struct evscgi *scgi, evutil_socket_t fd)
{
  /* Based on evhttp_accept_socket */
  struct evscgi_bound_socket *bound = evscgi_accept_socket_with_handle(scgi, fd);
  if (bound == NULL)
    return (-1);
  return (0);
}

struct evscgi_bound_socket *evscgi_accept_socket_with_handle(struct evscgi *scgi, evutil_socket_t fd)
{
  /* Based on evhttp_accept_socket_with_handle */
  struct evscgi_bound_socket *bound;
  struct evconnlistener *listener;
  const int flags = LEV_OPT_REUSEABLE|LEV_OPT_CLOSE_ON_EXEC|LEV_OPT_CLOSE_ON_FREE;
  
  listener = evconnlistener_new(scgi->base, NULL, NULL, flags, 0, fd);
  
  if (!listener)
    return (NULL);
  
  bound = evscgi_bind_listener(scgi, listener);
  if (!bound) {
    evconnlistener_free(listener);
    return (NULL);
  }
  return (bound);
}

struct evscgi_bound_socket *evscgi_bind_listener(struct evscgi *scgi, struct evconnlistener *listener)
{
  /* Based on evhttp_bind_listener */
  struct evscgi_bound_socket *bound;

  bound = malloc(sizeof(struct evscgi_bound_socket));
  if (bound == NULL)
    return (NULL);

  bound->listener = listener;
  TAILQ_INSERT_TAIL(&scgi->sockets, bound, next);

  evconnlistener_set_cb(listener, accept_socket_cb, scgi);
  return bound;
}

evutil_socket_t evscgi_bound_socket_get_fd(struct evscgi_bound_socket *bound)
{
  // Based on evhttp_bound_socket_get_fd
  return evconnlistener_get_fd(bound->listener);
}

struct evconnlistener *evscgi_bound_socket_get_listener(struct evscgi_bound_socket *bound)
{
  // Based on evhttp_bound_socket_get_listener
  return bound->listener;
}

void evscgi_del_accept_socket(struct evscgi *scgi, struct evscgi_bound_socket *bound)
{
  // Based on evhttp_del_accept_socket
  TAILQ_REMOVE(&scgi->sockets, bound, next);
  evconnlistener_free(bound->listener);
  free(bound);
}

void evscgi_connection_free(struct evscgi_connection *evcon)
{
  /* Based on evhttp_connection_free */
  struct evscgi_request *req;

  /* notify interested parties that this connection is going down */
  if (evcon->fd != -1) {
    if (evscgi_connected(evcon) && evcon->closecb != NULL)
      (*evcon->closecb)(evcon, evcon->closecb_arg);
  }

  /* remove all requests that might be queued on this
   * connection.  for server connections, this should be empty.
   * because it gets dequeued either in evscgi_connection_done or
   * evscgi_connection_fail.
   */
  while ((req = TAILQ_FIRST(&evcon->requests)) != NULL) {
    TAILQ_REMOVE(&evcon->requests, req, next);
    evscgi_request_free(req);
  }

  if (evcon->scgi_server != NULL) {
    struct evscgi *scgi = evcon->scgi_server;
    TAILQ_REMOVE(&scgi->connections, evcon, next);
  }

  if (event_initialized(&evcon->retry_ev)) {
    event_del(&evcon->retry_ev);
    event_debug_unassign(&evcon->retry_ev);
  }

  if (evcon->bufev != NULL)
    bufferevent_free(evcon->bufev);

//   event_deferred_cb_cancel(event_base_get_deferred_cb_queue(evcon->base),
//       &evcon->read_more_deferred_cb);

  if (evcon->fd != -1) {
    shutdown(evcon->fd, SHUT_WR);
    evutil_closesocket(evcon->fd);
  }

//   if (evcon->bind_address != NULL)
//     mm_free(evcon->bind_address);
// 
//   if (evcon->address != NULL)
//     mm_free(evcon->address);

  free(evcon);
}

/** Helper: returns true iff evconn is in any connected state. */
static bool evscgi_connected(const struct evscgi_connection *evcon)
{
  // Based on evhttp_connected
  switch (evcon->state)
  {
  case EVCON_DISCONNECTED:
//   case EVCON_CONNECTING:
    return false;
//   case EVCON_IDLE:
//   case EVCON_READING_FIRSTLINE:
  case EVCON_READING_HEADERS:
  case EVCON_READING_BODY:
//   case EVCON_READING_TRAILER:
  case EVCON_WRITING:
  default:
    return true;
  }
}

void evscgi_free(struct evscgi* scgi)
{
  /* Based on evhttp_free */
  struct evscgi_cb *scgi_cb;
  struct evscgi_connection *evcon;
  struct evscgi_bound_socket *bound;
  // struct evhttp* vhost;
  // struct evhttp_server_alias *alias;
  Word_t unused; // return value from dictionary destructor function

  /* Remove the accepting part */
  while ((bound = TAILQ_FIRST(&scgi->sockets)) != NULL) {
    TAILQ_REMOVE(&scgi->sockets, bound, next);

    evconnlistener_free(bound->listener);

    free(bound);
  }

  while ((evcon = TAILQ_FIRST(&scgi->connections)) != NULL) {
    /* evscgi_connection_free removes the connection */
    evscgi_connection_free(evcon);
  }

  while ((scgi_cb = TAILQ_FIRST(&scgi->callbacks)) != NULL) {
    TAILQ_REMOVE(&scgi->callbacks, scgi_cb, next);
    free(scgi_cb->what);
    free(scgi_cb);
  }

//   while ((vhost = TAILQ_FIRST(&http->virtualhosts)) != NULL) {
//     TAILQ_REMOVE(&http->virtualhosts, vhost, next_vhost);
// 
//     evhttp_free(vhost);
//   }
// 
//   if (http->vhost_pattern != NULL)
//     mm_free(http->vhost_pattern);
// 
//   while ((alias = TAILQ_FIRST(&http->aliases)) != NULL) {
//     TAILQ_REMOVE(&http->aliases, alias, next);
//     mm_free(alias->alias);
//     mm_free(alias);
//   }

  JSLFA(unused, scgi->hdrparsers_by_name);
  JSLFA(unused, scgi->methods_by_name);

  free(scgi);
}

int evscgi_set_cb(struct evscgi *scgi, const char *path, void(*cb)(struct evscgi_request *, void *), void *cbarg)
{
  /* Based on evhttp_set_cb */
  struct evscgi_cb *scgi_cb;

  TAILQ_FOREACH(scgi_cb, &scgi->callbacks, next) {
    if (strcmp(scgi_cb->what, path) == 0)
      return (-1);
  }

  if ((scgi_cb = calloc(1, sizeof(struct evscgi_cb))) == NULL) {
//     event_warn("%s: calloc", __func__);
    return (-2);
  }

  scgi_cb->what = strdup(path);
  if (scgi_cb->what == NULL) {
//     event_warn("%s: strdup", __func__);
    free(scgi_cb);
    return (-3);
  }
  scgi_cb->cb = cb;
  scgi_cb->cbarg = cbarg;

  TAILQ_INSERT_TAIL(&scgi->callbacks, scgi_cb, next);

  return (0);
}

int evscgi_del_cb(struct evscgi *scgi, const char *uri)
{
  /* Based on evhttp_del_cb */
  struct evscgi_cb *scgi_cb;

  TAILQ_FOREACH(scgi_cb, &scgi->callbacks, next) {
    if (strcmp(scgi_cb->what, uri) == 0)
      break;
  }
  if (scgi_cb == NULL)
    return (-1);

  TAILQ_REMOVE(&scgi->callbacks, scgi_cb, next);
  free(scgi_cb->what);
  free(scgi_cb);

  return (0);
}

void evscgi_set_gencb(struct evscgi *scgi, void(*cb)(struct evscgi_request *, void *), void *cbarg)
{
  /* Based on evhttp_set_gencb */
  scgi->gencb = cb;
  scgi->gencbarg = cbarg;
}

struct evscgi_connection *evscgi_unix_connection_base_new(struct event_base *base, const char *pathname)
{
  /* Based on evhttp_connection_base_new */
  struct evscgi_connection *evcon = NULL;

//   event_debug(("Attempting connection to %s:%d\n", address, port));

  if (base == NULL)
    goto error;
  
  if ((evcon = calloc(1, sizeof(struct evscgi_connection))) == NULL)
//     event_warn("%s: calloc failed", __func__);
    goto error;

  evcon->fd = -1;
//   evcon->port = port;

  evcon->max_headers_size = EV_SIZE_MAX;
  evcon->max_body_size = EV_SIZE_MAX;

  evcon->timeout = -1;
  evcon->retry_cnt = evcon->retry_max = 0;

  if ((evcon->path = strdup(pathname)) == NULL)
//     event_warn("%s: strdup failed", __func__);
    goto error;
  // TODO: Maybe split the setting of evcon->path out into a wrapper function; if/when TCP support is implemented, add another wrapper to set the address, port, and dnsbase

  if ((evcon->bufev = bufferevent_socket_new(base, -1, 0)) == NULL)
//     event_warn("%s: bufferevent_new failed", __func__);
    goto error;
  
  bufferevent_setcb(evcon->bufev,
                    evscgi_read_cb,
                    evscgi_write_cb,
                    evscgi_error_cb, evcon);

  evcon->state = EVCON_DISCONNECTED;
  TAILQ_INIT(&evcon->requests);

//   if (base != NULL) {
  evcon->base = base;
//     bufferevent_base_set(base, evcon->bufev);
//   }


//   event_deferred_cb_init(&evcon->read_more_deferred_cb,
//       evscgi_deferred_read_cb, evcon);

//   evcon->dns_base = dnsbase;

  return (evcon);

 error:
  if (evcon != NULL)
    evscgi_connection_free(evcon);
  return (NULL);
}

void evscgi_send_reply(struct evscgi_request *req, int code, const char *reason, struct evbuffer *databuf)
{
  /* Based on evhttp_send_reply */
  evscgi_response_code(req, code, reason);

  evscgi_send(req, databuf);
}

void evscgi_send_reply_start(struct evscgi_request *req, int code, const char *reason)
{
  /* Based on evhttp_send_reply_start */
  evscgi_response_code(req, code, reason);
  if (evhttp_find_header(req->output_headers, "Content-Length") == NULL &&
      REQ_VERSION_ATLEAST(req, 1, 1) &&
      evscgi_response_needs_body(req)) {
    /*
     * prefer HTTP/1.1 chunked encoding to closing the connection;
     * note RFC 2616 section 4.4 forbids it with Content-Length:
     * and it's not necessary then anyway.
     */
    evhttp_add_header(req->output_headers, "Transfer-Encoding", "chunked");
    req->chunked = 1;
  } else {
    req->chunked = 0;
  }
  evscgi_make_header(req->evcon, req);
  evscgi_write_buffer(req->evcon, NULL, NULL);
}

void evscgi_send_reply_chunk(struct evscgi_request *req, struct evbuffer *databuf)
{
  /* Based on evhttp_send_reply_chunk */
  struct evscgi_connection *evcon = req->evcon;
  struct evbuffer *output;

  if (evcon == NULL)
    return;

  output = bufferevent_get_output(evcon->bufev);

  if (evbuffer_get_length(databuf) == 0)
    return;
  if (!evscgi_response_needs_body(req))
    return;
  if (req->chunked) {
    evbuffer_add_printf(output, "%x\r\n",
            (unsigned)evbuffer_get_length(databuf));
  }
  evbuffer_add_buffer(output, databuf);
  if (req->chunked) {
    evbuffer_add(output, "\r\n", 2);
  }
  evscgi_write_buffer(evcon, NULL, NULL);
}

void evscgi_send_reply_end(struct evscgi_request *req)
{
  /* Based on evhttp_send_reply_end */
  struct evscgi_connection *evcon = req->evcon;
  struct evbuffer *output;

  if (evcon == NULL) {
    evscgi_request_free(req);
    return;
  }

  output = bufferevent_get_output(evcon->bufev);

  /* we expect no more calls form the user on this request */
  req->userdone = 1;

  if (req->chunked) {
    evbuffer_add(output, "0\r\n\r\n", 5);
    evscgi_write_buffer(req->evcon, evscgi_send_done, NULL);
    req->chunked = 0;
  } else if (evbuffer_get_length(output) == 0) {
    /* let the connection know that we are done with the request */
    evscgi_send_done(evcon, NULL);
  } else {
    /* make the callback execute after all data has been written */
    evcon->cb = evscgi_send_done;
    evcon->cb_arg = NULL;
  }
}

void evscgi_send_header(struct evscgi_request *req, const char *key, const char *value)
{
  // TODO: implement piecemeal sending of response headers (keeping this function prototype unchanged if possible)
  evhttp_add_header(req->output_headers, key, value);
}

void evscgi_response_code(struct evscgi_request *req, int code, const char *reason)
{
  /* Based on evhttp_response_code */
  req->kind = EVSCGI_RESPONSE;
  req->response_code = code;
  if (req->response_code_line != NULL)
    free(req->response_code_line);
  if (reason == NULL)
    reason = evscgi_response_phrase_internal(code);
  req->response_code_line = strdup(reason);
  if (req->response_code_line == NULL) {
//     event_warn("%s: strdup", __func__);
    /* XXX what else can we do? */
  }
}

// Next are the header-manipulation functions.
// NOTE: Because these don't take any object other than an evkeyvalq, we actually could use the ones from evhttp, correct? We'll try doing that. (However, if we were to add support for the client side of SCGI, maybe that would need different header-processing logic.)
// const char *evscgi_find_header(const struct evkeyvalq *headers, const char *key)
// {
//   // Based on evhttp_find_header
//   struct evkeyval *header;
// 
//   TAILQ_FOREACH(header, headers, next) {
//     if (evutil_ascii_strcasecmp(header->key, key) == 0)
//       return (header->value);
//   }
// 
//   return (NULL);
// }
// 
// void evscgi_clear_headers(struct evkeyvalq *headers)
// {
//   // Based on evhttp_clear_headers
//   struct evkeyval *header;
// 
//   for (header = TAILQ_FIRST(headers);
//       header != NULL;
//       header = TAILQ_FIRST(headers)) {
//     TAILQ_REMOVE(headers, header, next);
//     free(header->key);
//     free(header->value);
//     free(header);
//   }
// }
// 
// /*
//  * Returns 0,  if the header was successfully removed.
//  * Returns -1, if the header could not be found.
//  */
// 
// int evscgi_remove_header(struct evkeyvalq *headers, const char *key)
// {
//   // Based on evhttp_remove_header
//   struct evkeyval *header;
// 
//   TAILQ_FOREACH(header, headers, next) {
//     if (evutil_ascii_strcasecmp(header->key, key) == 0)
//       break;
//   }
// 
//   if (header == NULL)
//     return (-1);
// 
//   /* Free and remove the header that we found */
//   TAILQ_REMOVE(headers, header, next);
//   free(header->key);
//   free(header->value);
//   free(header);
// 
//   return (0);
// }
// 
// 
// int evscgi_add_header(struct evkeyvalq *headers, const char *key, const char *value)
// {
//   // Based on evhttp_add_header
// //   event_debug(("%s: key: %s val: %s\n", __func__, key, value));
// 
//   if (strchr(key, '\r') != NULL || strchr(key, '\n') != NULL) {
//     /* drop illegal headers */
// //     event_debug(("%s: dropping illegal header key\n", __func__));
//     return (-1);
//   }
// 
//   if (!evscgi_header_is_valid_value(value)) {
// //     event_debug(("%s: dropping illegal header value\n", __func__));
//     return (-1);
//   }
// 
//   return (evscgi_add_header_internal(headers, key, value));
// }

void evscgi_connection_set_timeout(struct evscgi_connection *evcon, int timeout_in_secs)
{
  // Based on evhttp_connection_set_timeout
  evcon->timeout = timeout_in_secs;

  if (evcon->timeout == -1)
    bufferevent_settimeout(evcon->bufev, SCGI_READ_TIMEOUT, SCGI_WRITE_TIMEOUT);
  else
    bufferevent_settimeout(evcon->bufev, evcon->timeout, evcon->timeout);
}

/*
 * Request related functions
 */

// struct evscgi_request *evscgi_request_new(void (*cb)(struct evscgi_request *, void *), void *arg)
static struct evscgi_request *evscgi_request_new()
{
  // Based on evhttp_request_new
  // NOTE: maybe we should merge this into another function?
  struct evscgi_request *req = NULL;

  /* Allocate request structure */
  if ((req = calloc(1, sizeof(struct evscgi_request))) == NULL) {
//     event_warn("%s: calloc", __func__);
    goto error;
  }

  req->headers_size = 0;
  req->body_size = 0;

  req->kind = EVSCGI_RESPONSE;
  req->input_headers = calloc(1, sizeof(struct evkeyvalq));
  if (req->input_headers == NULL) {
//     event_warn("%s: calloc", __func__);
    goto error;
  }
  TAILQ_INIT(req->input_headers);

  req->output_headers = calloc(1, sizeof(struct evkeyvalq));
  if (req->output_headers == NULL) {
//     event_warn("%s: calloc", __func__);
    goto error;
  }
  TAILQ_INIT(req->output_headers);

  if ((req->input_buffer = evbuffer_new()) == NULL) {
//     event_warn("%s: evbuffer_new", __func__);
    goto error;
  }

  if ((req->output_buffer = evbuffer_new()) == NULL) {
//     event_warn("%s: evbuffer_new", __func__);
    goto error;
  }

  // Removed the internal callback
//   req->cb = cb;
//   req->cb_arg = arg;

  return (req);

 error:
  if (req != NULL)
    evscgi_request_free(req);
  return (NULL);
}

static void evscgi_request_free(struct evscgi_request *req)
{
  // Based on evhttp_request_free
  if ((req->flags & EVSCGI_REQ_DEFER_FREE) != 0) {
    req->flags |= EVSCGI_REQ_NEEDS_FREE;
    return;
  }

//   if (req->remote_host != NULL)
//     mm_free(req->remote_host);
  if (req->uri != NULL)
    free(req->uri);
  if (req->uri_elems != NULL)
    evhttp_uri_free(req->uri_elems);
  if (req->response_code_line != NULL)
    free(req->response_code_line);
  if (req->host_cache != NULL)
    free(req->host_cache);

  evhttp_clear_headers(req->input_headers);
  free(req->input_headers);

  evhttp_clear_headers(req->output_headers);
  free(req->output_headers);

  if (req->input_buffer != NULL)
    evbuffer_free(req->input_buffer);

  if (req->output_buffer != NULL)
    evbuffer_free(req->output_buffer);

  free(req);
}

void evscgi_request_set_chunked_cbs(struct evscgi_request *req, void (*chunk_cb)(struct evscgi_request *, void *), void (*chunksdone_cb)(struct evscgi_request *, void *), void *chunk_cb_arg)
{
  req->chunk_cb = chunk_cb;
  req->chunksdone_cb = chunksdone_cb;
  req->chunk_cb_arg = chunk_cb_arg;
}

void evscgi_request_set_header_cb(struct evscgi_request *req, void (*header_cb)(struct evscgi_request *, void *, const char *, const char *), void (*headersdone_cb)(struct evscgi_request *, void *), void *header_cb_arg)
{
  req->header_cb = header_cb;
  req->headersdone_cb = headersdone_cb;
  req->header_cb_arg = header_cb_arg;
}

void evscgi_set_timeout(struct evscgi* scgi, int timeout_in_secs)
{
  // Based on evhttp_set_timeout
  scgi->timeout = timeout_in_secs;
}

void evscgi_set_max_headers_size(struct evscgi* scgi, ev_ssize_t max_headers_size)
{
  // Based on evhttp_set_max_headers_size
  if (max_headers_size < 0)
    scgi->default_max_headers_size = EV_SIZE_MAX;
  else
    scgi->default_max_headers_size = max_headers_size;
}

void evscgi_set_max_body_size(struct evscgi* scgi, ev_ssize_t max_body_size)
{
  // Based on evhttp_set_max_body_size
  if (max_body_size < 0)
    scgi->default_max_body_size = EV_UINT64_MAX;
  else
    scgi->default_max_body_size = max_body_size;
}

/*
 * Returns an error page.
 */
void evscgi_send_error(struct evscgi_request *req, int error, const char *reason)
{
  // Based on evhttp_send_error

#define ERR_FORMAT "<HTML><HEAD>\n" \
      "<TITLE>%d %s</TITLE>\n" \
      "</HEAD><BODY>\n" \
      "<H1>%s</H1>\n" \
      "</BODY></HTML>\n"

  struct evbuffer *buf = evbuffer_new();
  if (buf == NULL) {
    /* if we cannot allocate memory; we just drop the connection */
    evscgi_connection_free(req->evcon);
    return;
  }
  if (reason == NULL) {
    reason = evscgi_response_phrase_internal(error);
  }

  evscgi_response_code(req, error, reason);

  evbuffer_add_printf(buf, ERR_FORMAT, error, reason, reason);

  evscgi_send_page(req, buf);

  evbuffer_free(buf);
#undef ERR_FORMAT
}

// Getter-type functions for evscgi_request here:
enum evscgi_cmd_type evscgi_request_get_command(const struct evscgi_request *req)
{
  // Based on evhttp_request_get_command
  return (req->type);
}

struct evscgi_connection *evscgi_request_get_connection(struct evscgi_request *req)
{
  // Based on evhttp_request_get_connection
  return req->evcon;
}

const struct evhttp_uri *evscgi_request_get_evhttp_uri(const struct evscgi_request *req)
{
  // Based on evhttp_request_get_evhttp_uri
//   if (req->uri_elems == NULL)
//     event_debug(("%s: request %p has no uri elems\n",
//           __func__, req));
  return (req->uri_elems);
}

  // Based on evhttp_request_get_host
const char *evscgi_request_get_host(struct evscgi_request *req)
{
  const char *host = NULL;

  if (req->host_cache)
    return req->host_cache;

  if (req->uri_elems)
    host = evhttp_uri_get_host(req->uri_elems);
  if (!host && req->input_headers) {
    const char *p;
    size_t len;

    host = evhttp_find_header(req->input_headers, "HTTP_HOST");
    /* The Host: header may include a port. Remove it here
       to be consistent with uri_elems case above. */
    if (host) {
      p = host + strlen(host) - 1;
      // NOTE: evhttp uses the ASCII-only function EVUTIL_ISDIGIT, but that's an internal-only libevent function.
      while (p > host && isdigit(*p))
        --p;
      if (p > host && *p == ':') {
        len = p - host;
        req->host_cache = malloc(len + 1);
        if (!req->host_cache) {
//           event_warn("%s: malloc", __func__);
          return NULL;
        }
        memcpy(req->host_cache, host, len);
        req->host_cache[len] = '\0';
        host = req->host_cache;
      }
    }
  }

  return host;
}

/** Returns the input buffer */
struct evbuffer *evscgi_request_get_input_buffer(struct evscgi_request *req)
{
  // Based on evhttp_request_get_input_buffer
  return (req->input_buffer);
}

/** Returns the input headers */
struct evkeyvalq *evscgi_request_get_input_headers(struct evscgi_request *req)
{
  // Based on evhttp_request_get_input_headers
  return (req->input_headers);
}

/** Returns the output buffer */
struct evbuffer *evscgi_request_get_output_buffer(struct evscgi_request *req)
{
  // Based on evhttp_request_get_output_buffer
  return (req->output_buffer);
}

/** Returns the output headers */
struct evkeyvalq *evscgi_request_get_output_headers(struct evscgi_request *req)
{
  // Based on evhttp_request_get_output_headers
  return (req->output_headers);
}

int evscgi_request_get_response_code(const struct evscgi_request *req)
{
  // Based on evhttp_request_get_response_code
  return req->response_code;
}

/*
 * Allows for inspection of the request URI
 */

const char *evscgi_request_get_uri(const struct evscgi_request *req)
{
  // Based on evhttp_request_get_uri
//   if (req->uri == NULL)
//     event_debug(("%s: request %p has no uri\n", __func__, req));
  return (req->uri);
}


void evscgi_cancel_request(struct evscgi_request *req)
{
  // Based on evhttp_cancel_request
  struct evscgi_connection *evcon = req->evcon;
  if (evcon != NULL) {
    /* We need to remove it from the connection */
    if (TAILQ_FIRST(&evcon->requests) == req) {
      /* it's currently being worked on, so reset
       * the connection.
       */
      evscgi_connection_fail(evcon,
          EVCON_SCGI_REQUEST_CANCEL);

      /* connection fail freed the request */
      return;
    } else {
      /* otherwise, we can just remove it from the
       * queue
       */
      TAILQ_REMOVE(&evcon->requests, req, next);
    }
  }

  evscgi_request_free(req);
}

struct event_base *evscgi_connection_get_base(struct evscgi_connection *conn)
{
  // Based on evhttp_connection_get_base
  return conn->base;
}

void evscgi_connection_set_closecb(struct evscgi_connection *evcon, void (*cb)(struct evscgi_connection *, void *), void *cbarg)
{
  // Based on evhttp_connection_set_closecb
  evcon->closecb = cb;
  evcon->closecb_arg = cbarg;
}

void evscgi_connection_set_max_body_size(struct evscgi_connection* evcon, ev_ssize_t new_max_body_size)
{
  // Based on evhttp_connection_set_max_body_size
  if (new_max_body_size<0)
    evcon->max_body_size = EV_UINT64_MAX;
  else
    evcon->max_body_size = new_max_body_size;
}

void evscgi_connection_set_max_headers_size(struct evscgi_connection *evcon, ev_ssize_t new_max_headers_size)
{
  // Based on evhttp_connection_set_max_headers_size
  if (new_max_headers_size<0)
    evcon->max_headers_size = EV_SIZE_MAX;
  else
    evcon->max_headers_size = new_max_headers_size;
}

void evscgi_request_own(struct evscgi_request *req)
{
  // Based on evhttp_request_own
  req->flags |= EVSCGI_USER_OWNED;
}

void evscgi_set_allowed_methods(struct evscgi* scgi, ev_uint16_t methods)
{
  // Based on evhttp_set_allowed_methods
  scgi->allowed_methods = methods;
}

void evscgi_set_notify_on(struct evscgi *scgi, enum evscgi_notify_mode when)
{
  scgi->notify_on = when;
}

// NOTE: The public API functions end here; below are the internal-only functions:

static evutil_socket_t bind_unix_socket(const char *pathname, int reuse)
{
  // Based on bind_socket_ai
  evutil_socket_t fd;
  
  int on = 1, r;
  int serrno;
  
  struct sockaddr_un addr;

  /* Create listen socket */
  fd = socket(AF_UNIX, SOCK_STREAM, 0);
  if (fd == -1) {
//       event_sock_warn(-1, "socket");
      return (-1);
  }

  if (evutil_make_socket_nonblocking(fd) < 0)
    goto out;
  if (evutil_make_socket_closeonexec(fd) < 0)
    goto out;

  // I don't think that SO_KEEPALIVE has any effect on AF_UNIX sockets.
//   setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (void *)&on, sizeof(on));
  if (reuse)
    evutil_make_listen_socket_reuseable(fd);

//   if (ai != NULL) {
  if (pathname != NULL) {
//     r = bind(fd, ai->ai_addr, (ev_socklen_t)ai->ai_addrlen);
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, pathname, sizeof(addr.sun_path) - 1);
    addr.sun_path[sizeof(addr.sun_path) - 1] = '\0';
    r = bind(fd, (struct sockaddr *) &addr, sizeof(addr));
    if (r == -1)
      goto out;
  }

  return (fd);

 out:
  serrno = EVUTIL_SOCKET_ERROR();
  evutil_closesocket(fd);
  EVUTIL_SET_SOCKET_ERROR(serrno);
  return (-1);
  
  
  return (fd);
}

/*
 * Takes a file descriptor to read a request from.
 * The callback is executed once the whole request has been read.
 */

static struct evscgi_connection *evscgi_get_request_connection(struct evscgi* scgi, evutil_socket_t fd, struct sockaddr *sa, ev_socklen_t salen)
{
  /* Based on evhttp_get_request_connection */
  struct evscgi_connection *evcon;
//   char *hostname = NULL, *portname = NULL;
  char *pathname = NULL;

//   name_from_addr(sa, salen, &hostname, &portname);
  // NOTE: getnameinfo retrieves path as "serv" (instead of the port), maybe?
  // NOTE: trying it in Python, the address returned by accept(2) on a Unix domain socket was an empty string, and I couldn't seem to use getnameinfo(3) on it.
  if (sa->sa_family == AF_UNIX)
  {
    pathname = strndup(((struct sockaddr_un *) sa)->sun_path, sizeof(((struct sockaddr_un *) sa)->sun_path));
    evcon = evscgi_unix_connection_base_new(scgi->base, pathname);
    free(pathname);
  }
  else
    // TODO: allow it to use TCP sockets in addition to Unix domain sockets
    return (NULL);
//   if (hostname == NULL || portname == NULL) {
//     if (hostname) mm_free(hostname);
//     if (portname) mm_free(portname);
//     return (NULL);
//   }

//   event_debug(("%s: new request from %s:%s on %d\n",
//       __func__, hostname, portname, fd));

  /* we need a connection object to put the scgi request on */
//   evcon = evhttp_connection_base_new(
//     http->base, NULL, hostname, atoi(portname));
//   mm_free(hostname);
//   mm_free(portname);
  if (evcon == NULL)
    return (NULL);

  evcon->max_headers_size = scgi->default_max_headers_size;
  evcon->max_body_size = scgi->default_max_body_size;

  evcon->flags |= EVSCGI_CON_INCOMING;
  evcon->state = EVCON_READING_HEADERS;

  evcon->fd = fd;

  bufferevent_setfd(evcon->bufev, fd);

  return (evcon);
}

static int evscgi_associate_new_request_with_connection(struct evscgi_connection *evcon)
{
  /* Based on evhttp_associate_new_request_with_connection */
  struct evscgi *scgi = evcon->scgi_server;
  struct evscgi_request *req;
  if ((req = evscgi_request_new()) == NULL)
    return (-1);

//   if ((req->remote_host = mm_strdup(evcon->address)) == NULL) {
//     event_warn("%s: strdup", __func__);
//     evscgi_request_free(req);
//     return (-1);
//   }
//   req->remote_port = evcon->port;

  req->evcon = evcon; /* the request ends up owning the connection */
  req->flags |= EVSCGI_REQ_OWN_CONNECTION;

  if (evcon->scgi_server->notify_on == EVSCGI_NOTIFY_NEW_CONNECTION)
  {
    // notify the user of request
//     (*req->cb)(req, req->cb_arg);
//     // The above call is actually calling evscgi_handle_request. Note that it's missing some functionality in EVSCGI_NOTIFY_NEW_CONNECTION that's present when in EVSCGI_NOTIFY_RESPONSE_NEEDED mode, because the request method and URI are not yet available. This means it can't call the per-URI callbacks, but only the generic one; also, it can't filter out requests by method.
    evscgi_handle_request(req, scgi);
  }
  else
  {
    /* We did not present the request to the user yet, so treat it as
     * if the user was done with the request.  This allows us to free the
     * request on a persistent connection if the client drops it without
     * sending a request.
     */
    req->userdone = 1;
  }

  TAILQ_INSERT_TAIL(&evcon->requests, req, next);

  req->kind = EVSCGI_REQUEST;


  evscgi_start_read(evcon);

  return (0);
}

/*
 * Reads data from file descriptor into request structure
 * Request structure needs to be set up correctly.
 */

static void evscgi_start_read(struct evscgi_connection *evcon)
{
  // Based on evhttp_start_read
  /* Set up an event to read the headers */
  bufferevent_disable(evcon->bufev, EV_WRITE);
  bufferevent_enable(evcon->bufev, EV_READ);
  evcon->state = EVCON_READING_HEADERS;
  /* Reset the bufferevent callbacks */
  bufferevent_setcb(evcon->bufev,
      evscgi_read_cb,
      evscgi_write_cb,
      evscgi_error_cb,
      evcon);

//   /* If there's still data pending, process it next time through the
//    * loop.  Don't do it now; that could get recusive. */
//   if (evbuffer_get_length(bufferevent_get_input(evcon->bufev))) {
  // NOTE: All of the libevent functions about deferred callbacks (including the two that were used below, event_deferred_cb_schedule and event_base_get_deferred_cb_queue) are libevent internal-only functions, so we shouldn't try to use them here. The correct way to schedule a deferred callback with the public libevent API is to create an event object (with event_new) and then manually activate it (with event_active). However, we don't actually need a deferred-read callback at all for the SCGI server, as explained below.
//     event_deferred_cb_schedule(event_base_get_deferred_cb_queue(evcon->base),
//         &evcon->read_more_deferred_cb);
//   }

  // The purpose of evhttp's deferred-read callback is to be used in client mode
  // when the remote server sends a "100 Continue" response. In that case, it calls
  // evhttp_start_read directly from evhttp_read_header (why? I think it might be to
  // make it once again read in and parse a "first line" followed by a batch of
  // headers, because there will be another "first line" and following headers in
  // case the truly-first "first line" was a 100 Continue), and then schedules the
  // read callback to be called next time through the event loop in order to read any
  // data from the server that's already in the buffer, without waiting for the next
  // read-from-socket event (i.e. for more newly-arrived data from the server). Note
  // that the read callback itself calls evhttp_read_header (which in turn can call
  // evhttp_start_read, as explained already); this is why calling the read callback
  // directly here could cause recursion, as the original comment above notes. But
  // since we're only implementing an SCGI server, we will never need to read an HTTP
  // "first line" (let alone more than one of them) from the remote side of the
  // connection, nor do we have any reason to reset evcon->state from an later state
  // to an earlier one, so we can eliminate the deferred-read callback entirely.
}

static void evscgi_get_request(struct evscgi *scgi, evutil_socket_t fd, struct sockaddr *sa, ev_socklen_t salen)
{
  /* Based on evhttp_get_request */
  struct evscgi_connection *evcon;

  evcon = evscgi_get_request_connection(scgi, fd, sa, salen);
  if (evcon == NULL) {
//     event_sock_warn(fd, "%s: cannot get connection on %d", __func__, fd);
    evutil_closesocket(fd);
    return;
  }

  /* the timeout can be used by the server to close idle connections */
  if (scgi->timeout != -1)
    evscgi_connection_set_timeout(evcon, scgi->timeout);

  /*
   * if we want to accept more than one request on a connection,
   * we need to know which http server it belongs to.
   */
  // TODO: but with SCGI, you can't accept more than one request on a connection; so maybe we can remove this part?
  evcon->scgi_server = scgi;
  TAILQ_INSERT_TAIL(&scgi->connections, evcon, next);

  if (evscgi_associate_new_request_with_connection(evcon) == -1)
    evscgi_connection_free(evcon);
}

static void evscgi_handle_request(struct evscgi_request *req, void *arg)
{
  // Based on evhttp_handle_request
  struct evscgi *scgi = arg;
  struct evscgi_cb *cb = NULL;
  const char *hostname;

  /* we have a new request on which the user needs to take action */
  req->userdone = 0;

  if (scgi->notify_on != EVSCGI_NOTIFY_NEW_CONNECTION)
  {
    // In notify-on-new-conneciton mode, the request URI and method are not yet available when we call the user's callback, so we can't filter out requests by method, nor can we use per-URI callbacks. (Only the generic callback can be used in this case.)
    
    if (!req->found_scgi_hdr || req->type == 0 || req->uri == NULL) {
      evscgi_send_error(req, HTTP_BADREQUEST, NULL);
      return;
    }

    if ((scgi->allowed_methods & req->type) == 0) {
  //     event_debug(("Rejecting disallowed method %x (allowed: %x)\n",
  //       (unsigned)req->type, (unsigned)http->allowed_methods));
      evscgi_send_error(req, HTTP_NOTIMPLEMENTED, NULL);
      return;
    }

    // We're not implementing virtual hosts in the SCGI server (the front-end HTTP server should have already done that)
  //   /* handle potential virtual hosts */
  //   hostname = evhttp_request_get_host(req);
  //   if (hostname != NULL) {
  //     evhttp_find_vhost(http, &http, hostname);
  //   }

    if ((cb = evscgi_dispatch_callback(&scgi->callbacks, req)) != NULL)
    {
      (*cb->cb)(req, cb->cbarg);
      return;
    }
  }
  
  if (scgi->gencb) /* Generic call back */
  {
    (*scgi->gencb)(req, scgi->gencbarg);
    return;
  }
  else
  {
    /* We need to send a 404 here */
#define ERR_FORMAT "<html><head>" \
        "<title>404 Not Found</title>" \
        "</head><body>" \
        "<h1>Not Found</h1>" \
        "<p>The requested URL %s was not found on this server.</p>"\
        "</body></html>\n"

    char *escaped_html;
    struct evbuffer *buf;

    if ((escaped_html = evhttp_htmlescape(req->uri)) == NULL) {
      evscgi_connection_free(req->evcon);
      return;
    }

    if ((buf = evbuffer_new()) == NULL) {
      free(escaped_html);
      evscgi_connection_free(req->evcon);
      return;
    }

    evscgi_response_code(req, HTTP_NOTFOUND, "Not Found");

    evbuffer_add_printf(buf, ERR_FORMAT, escaped_html);

    free(escaped_html);

    evscgi_send_page(req, buf);

    evbuffer_free(buf);
#undef ERR_FORMAT
  }
}

static struct evscgi_cb *evscgi_dispatch_callback(struct scgicbq *callbacks, struct evscgi_request *req)
{
  // Based on evhttp_dispatch_callback
  struct evscgi_cb *cb;
  size_t offset = 0;
  char *translated;
  const char *path;

  /* Test for different URLs */
  path = evhttp_uri_get_path(req->uri_elems);
  offset = strlen(path);
  if ((translated = malloc(offset + 1)) == NULL)
    return (NULL);
  evhttp_decode_uri_internal(path, offset, translated,
      0 /* decode_plus */);

  TAILQ_FOREACH(cb, callbacks, next) {
    if (!strcmp(cb->what, translated)) {
      free(translated);
      return (cb);
    }
  }

  free(translated);
  return (NULL);
}

/*
 * @param decode_plus_ctl: if 1, we decode plus into space.  If 0, we don't.
 *     If -1, when true we transform plus to space only after we've seen
 *     a ?.  -1 is deprecated.
 * @return the number of bytes written to 'ret'.
 */
static int evhttp_decode_uri_internal(const char *uri, size_t length, char *ret, int decode_plus_ctl)
{
  // This function is internal-only in libevent (even though it doesn't use evhttp structs), so we need to copy it here.
  char c;
  int j;
  int decode_plus = (decode_plus_ctl == 1) ? 1: 0;
  unsigned i;

  for (i = j = 0; i < length; i++) {
    c = uri[i];
    if (c == '?') {
      if (decode_plus_ctl < 0)
        decode_plus = 1;
    } else if (c == '+' && decode_plus) {
      c = ' ';
    } else if (c == '%' && isxdigit(uri[i+1]) && isxdigit(uri[i+2])) {
      // NOTE: evhttp uses the ASCII-only EVUTIL_ISXDIGIT, but that's an internal-only libevent function.
      char tmp[3];
      tmp[0] = uri[i+1];
      tmp[1] = uri[i+2];
      tmp[2] = '\0';
      c = (char)strtol(tmp, NULL, 16);
      i += 2;
    }
    ret[j++] = c;
  }
  ret[j] = '\0';

  return (j);
}

/* Listener callback when a connection arrives at a server. */
static void accept_socket_cb(struct evconnlistener *listener, evutil_socket_t nfd, struct sockaddr *peer_sa, int peer_socklen, void *arg)
{
  /* Based on accept_socket_cb from libevent (http.c) */
  struct evscgi *scgi = arg;

  evscgi_get_request(scgi, nfd, peer_sa, peer_socklen);
}

/* Next, we have the callbacks for I/O activity (bufferevent) */

/*
 * Gets called when more data becomes available
 */

static void evscgi_read_cb(struct bufferevent *bufev, void *arg)
{
  /* Based on evscgi_read_cb */
  struct evscgi_connection *evcon = arg;
  struct evscgi_request *req = TAILQ_FIRST(&evcon->requests);

//   /* Cancel if it's pending. */
//   event_deferred_cb_cancel(event_base_get_deferred_cb_queue(evcon->base),
//       &evcon->read_more_deferred_cb);

  switch (evcon->state) {
  case EVCON_READING_HEADERS:
    evscgi_read_header(evcon, req);
    /* note the request may have been freed in
     * evscgi_read_body */
    break;
  case EVCON_READING_BODY:
    evscgi_read_body(evcon, req);
    /* note the request may have been freed in
     * evscgi_read_body */
    break;
//   case EVCON_READING_TRAILER:
//     evscgi_read_trailer(evcon, req);
//     break;
//   case EVCON_IDLE:
//     {
//       // For now, we don't have any way of printing debug messages, so comment this out:
// #ifdef USE_DEBUG
//       struct evbuffer *input;
//       size_t total_len;
// 
//       input = bufferevent_get_input(evcon->bufev);
//       total_len = evbuffer_get_length(input);
//       event_debug(("%s: read "EV_SIZE_FMT
//         " bytes in EVCON_IDLE state,"
//         " resetting connection",
//         __func__, EV_SIZE_ARG(total_len)));
// #endif
// 
//       evscgi_connection_reset(evcon);
//     }
//     break;
  case EVCON_DISCONNECTED:
//   case EVCON_CONNECTING:
  case EVCON_WRITING:
  default:
//     event_errx(1, "%s: illegal connection state %d",
//          __func__, evcon->state);
    break;
  }
}

// static void evscgi_deferred_read_cb(struct deferred_cb *cb, void *data)
// {
//   // Based on evhttp_deferred_read_cb
//   struct evscgi_connection *evcon = data;
//   evscgi_read_cb(evcon->bufev, evcon);
// }

/* Bufferevent callback: invoked when any data has been written from an
 * http connection's bufferevent */
static void evscgi_write_cb(struct bufferevent *bufev, void *arg)
{
  // Based on evhttp_write_cb
  struct evscgi_connection *evcon = arg;

  /* Activate our call back */
  if (evcon->cb != NULL)
    (*evcon->cb)(evcon, evcon->cb_arg);
}

static void evscgi_error_cb(struct bufferevent *bufev, short what, void *arg)
{
  // Based on evhttp_error_cb
  struct evscgi_connection *evcon = arg;
  struct evscgi_request *req = TAILQ_FIRST(&evcon->requests);

  switch (evcon->state) {
    // The "connecting" state only makes sense when we are a client, not a server, and client mode isn't implemented now.
//   case EVCON_CONNECTING:
//     if (what & BEV_EVENT_TIMEOUT) {
//       event_debug(("%s: connection timeout for \"%s:%d\" on %d",
//         __func__, evcon->address, evcon->port,
//         evcon->fd));
//       evhttp_connection_cb_cleanup(evcon);
//       return;
//     }
//     break;

  case EVCON_READING_BODY:
    if (what == (BEV_EVENT_READING|BEV_EVENT_EOF))
    {
      // EOF on read can be benign (and unlike in HTTP, request can't have Transfer-Encoding: chunked)
      // TODO: WAIT! Better check that this really makes sense in SCGI! Notice this the original code tested for (!req->chunked && req->ntoread < 0), which means that the request or response has neither "Transfer-Encoding: chunked" nor any Content-Length specified in the headers; in that case, the only way for remote end to indicate the end of the body is to shut down its write half of the connection, which is what this code here is no doubt checking for. In SCGI, however, the client should NEVER shut down any part of the connection; when the client finshes sending the request, it's supposed to leave the connection fully open and read the response, and the server is supposed to shut down the whole connection after it's fully sent the response. Therefore, if we read an EOF while in EVCON_READING_BODY state (i.e. we get to here in the code), that means we're dealing with a buggy SCGI client; but maybe it can still correctly read and process a response if it has only shut down its write half?
      evscgi_connection_done(evcon);
      return;
    }
    break;

  case EVCON_DISCONNECTED:
//   case EVCON_IDLE:
//   case EVCON_READING_FIRSTLINE:
  case EVCON_READING_HEADERS:
//   case EVCON_READING_TRAILER:
  case EVCON_WRITING:
  default:
    break;
  }

  // NOTE: Could "close detect mode" be applicable to SCGI? I doubt it -- either we should always do this or never do this, and I'm guessing never.
//   /* when we are in close detect mode, a read error means that
//    * the other side closed their connection.
//    */
//   if (evcon->flags & EVHTTP_CON_CLOSEDETECT) {
//     evcon->flags &= ~EVHTTP_CON_CLOSEDETECT;
//     EVUTIL_ASSERT(evcon->http_server == NULL);
//     /* For connections from the client, we just
//      * reset the connection so that it becomes
//      * disconnected.
//      */
//     EVUTIL_ASSERT(evcon->state == EVCON_IDLE);
//     evhttp_connection_reset(evcon);
//     return;
//   }

  if (what & BEV_EVENT_TIMEOUT) {
    // NOTE: I think that the "timeout" error can only occur when trying to connect to a server (which we never do because we don't yet implement a client), but leave this in for now, just in case.
    evscgi_connection_fail(evcon, EVCON_SCGI_TIMEOUT);
  } else if (what & (BEV_EVENT_EOF|BEV_EVENT_ERROR)) {
    evscgi_connection_fail(evcon, EVCON_SCGI_EOF);
  } else {
    evscgi_connection_fail(evcon, EVCON_SCGI_BUFFER_ERROR);
  }
}

/**
 * Advance the connection state. We've just processed the request;
 *   respond.
 */
static void evscgi_connection_done(struct evscgi_connection *evcon)
{
  // Based on evhttp_connection_done
  struct evscgi_request *req = TAILQ_FIRST(&evcon->requests);
//   int con_outgoing = evcon->flags & EVHTTP_CON_OUTGOING;
// 
//   if (con_outgoing) {
//     /* idle or close the connection */
//     int need_close;
//     TAILQ_REMOVE(&evcon->requests, req, next);
//     req->evcon = NULL;
// 
//     evcon->state = EVCON_IDLE;
// 
//     need_close =
//         evhttp_is_connection_close(req->flags, req->input_headers)||
//         evhttp_is_connection_close(req->flags, req->output_headers);
// 
//     /* check if we got asked to close the connection */
//     if (need_close)
//       evhttp_connection_reset(evcon);
// 
//     if (TAILQ_FIRST(&evcon->requests) != NULL) {
//       /*
//        * We have more requests; reset the connection
//        * and deal with the next request.
//        */
//       if (!evhttp_connected(evcon))
//         evhttp_connection_connect(evcon);
//       else
//         evhttp_request_dispatch(evcon);
//     } else if (!need_close) {
//       /*
//        * The connection is going to be persistent, but we
//        * need to detect if the other side closes it.
//        */
//       evhttp_connection_start_detectclose(evcon);
//     }
//   } else {
//     /*
//      * incoming connection - we need to leave the request on the
//      * connection so that we can reply to it.
//      */
  evcon->state = EVCON_WRITING;
//   }

  if (evcon->scgi_server->notify_on == EVSCGI_NOTIFY_RESPONSE_NEEDED)
  {
    /* notify the user of the request */
//     (*req->cb)(req, req->cb_arg);
   evscgi_handle_request(req, evcon->scgi_server); 
  }

//   /* if this was an outgoing request, we own and it's done. so free it.
//    * unless the callback specifically requested to own the request.
//    */
//   if (con_outgoing && ((req->flags & EVHTTP_USER_OWNED) == 0)) {
//     evscgi_request_free(req);
//   }
}

/* Called when evcon has experienced a (non-recoverable? -NM) error, as
 * given in error. */
static void evscgi_connection_fail(struct evscgi_connection *evcon, enum evscgi_connection_error error)
{
  // Based on evhttp_connection_fail
  struct evscgi_request* req = TAILQ_FIRST(&evcon->requests);
//   void (*cb)(struct evhttp_request *, void *);
//   void *cb_arg;
  assert(req != NULL);

  bufferevent_disable(evcon->bufev, EV_READ|EV_WRITE);

//   if (evcon->flags & EVHTTP_CON_INCOMING) { // Currently we are only a server, so all connections are incoming ones.
  // TODO: Revise the comment below appropriately
  /*
   * for incoming requests, there are two different
   * failure cases.  it's either a network level error
   * or an http layer error. for problems on the network
   * layer like timeouts we just drop the connections.
   * For HTTP problems, we might have to send back a
   * reply before the connection can be freed.
   */
  if (evscgi_connection_incoming_fail(req, error) == -1)
    evscgi_connection_free(evcon);
  // Cut the rest of the original function, because it's for outgoing connections only.
}

// TODO: Maybe should merge evscgi_connection_incoming_fail into evscgi_connection_fail.
static int evscgi_connection_incoming_fail(struct evscgi_request *req, enum evscgi_connection_error error)
{
  // Based on evhttp_connection_incoming_fail
  switch (error) {
  case EVCON_SCGI_TIMEOUT:
  case EVCON_SCGI_EOF:
    /*
     * these are cases in which we probably should just
     * close the connection and not send a reply.  this
     * case may happen when a browser keeps a persistent
     * connection open and we timeout on the read.  when
     * the request is still being used for sending, we
     * need to disassociated it from the connection here.
     */
    if (!req->userdone) {
      /* remove it so that it will not be freed */
      TAILQ_REMOVE(&req->evcon->requests, req, next);
      /* indicate that this request no longer has a
       * connection object
       */
      req->evcon = NULL;
    }
    return (-1);
  case EVCON_SCGI_INVALID_HEADER:
  case EVCON_SCGI_BUFFER_ERROR:
  case EVCON_SCGI_REQUEST_CANCEL:
  default:  /* xxx: probably should just error on default */
    /* the callback looks at the uri to determine errors */
    if (req->uri) {
      free(req->uri);
      req->uri = NULL;
    }
    if (req->uri_elems) {
      evhttp_uri_free(req->uri_elems);
      req->uri_elems = NULL;
    }

//     /*
//      * the callback needs to send a reply, once the reply has
//      * been send, the connection should get freed.
//      */
//     (*req->cb)(req, req->cb_arg);
//     // NOTE: This call to the internal callback (which is actually evscgi_handle_request) will NEVER call the user-supplied gencb or pathname-associated cbs, because req->uri is NULL at the time it's called.
//     // TODO: I think we really should get rid of this "internal callback" and just call evscgi_handle_request directly. (Maybe evhttp was designed to let you use your own callback instead of that function, but I don't like that design.)
    // For now, I'm replacing that call with a call to evscgi_send_error, which is what I believe would actually have been done every time by the previous call in the case that req->uri is NULL.
    evscgi_send_error(req, HTTP_BADREQUEST, NULL);
  }

  return (0);
}
// NOTE: We're not implementing an equivalent of evhttp_connection_reset; it's inapplicable to SCGI, because there can only be one SCGI request per connection.

/* Next, we have the functions that parse the SCGI request and build the response */

static void evscgi_read_header(struct evscgi_connection *evcon, struct evscgi_request *req)
{
  /* Based on evhttp_read_header */
  enum message_read_status res;
  evutil_socket_t fd = evcon->fd;

  res = evscgi_parse_headers(req, bufferevent_get_input(evcon->bufev));
  if (res == DATA_CORRUPTED || res == DATA_TOO_LONG) {
    /* Error while reading, terminate */
//     event_debug(("%s: bad header lines on %d\n", __func__, fd));
    evscgi_connection_fail(evcon, EVCON_SCGI_INVALID_HEADER);
    return;
  } else if (res == MORE_DATA_EXPECTED) {
    /* Need more header lines */
    return;
  }

  /* Disable reading for now */
  bufferevent_disable(evcon->bufev, EV_READ);

  /* Done reading headers, do the real work */
//   switch (req->kind) {
//   case EVSCGI_REQUEST:
  // NOTE: evhttp had separate cases here based on the value of req->kind, but as long as we're only implementing the server side of SCGI, it doesn't make sense for req->kind to be anything other than EVSCGI_REQUEST here, right?
  assert(EVSCGI_REQUEST == req->kind);
//     event_debug(("%s: checking for post data on %d\n",
//         __func__, fd));
  evscgi_get_body(evcon, req);
    /* note the request may have been freed in evscgi_get_body */
//     break;
// 
//   case EVSCGI_RESPONSE:
//     /* Start over if we got a 100 Continue response. */
//     if (req->response_code == 100) {
//       evscgi_start_read(evcon);
//       return;
//     }
//     if (!evscgi_response_needs_body(req)) {
//       event_debug(("%s: skipping body for code %d\n",
//           __func__, req->response_code));
//       evscgi_connection_done(evcon);
//     } else {
//       event_debug(("%s: start of read body for %s on %d\n",
//         __func__, req->remote_host, fd));
//       evscgi_get_body(evcon, req);
//       /* note the request may have been freed in
//        * evhttp_get_body */
//     }
//     break;
// 
//   default:
//     event_warnx("%s: bad header on %d", __func__, fd);
//     evscgi_connection_fail(evcon, EVCON_SCGI_INVALID_HEADER);
//     break;
//   }
  /* request may have been freed above */
}

static enum message_read_status evscgi_parse_headers(struct evscgi_request *req, struct evbuffer* buffer)
{
  /* Based on evhttp_parse_headers */
  enum message_read_status errcode = DATA_CORRUPTED;
//   char *line;
//   char data[4097];
//   enum message_read_status status = MORE_DATA_EXPECTED;
  enum message_read_status status;

  struct evkeyvalq* headers = req->input_headers;
//   size_t line_length;
  size_t buffer_length;
//   while ((line = evbuffer_readln(buffer, &line_length, EVBUFFER_EOL_CRLF))
//          != NULL) {
//   data[sizeof(data) - 1)] = '\0';
//   while ((data_length = evbuffer_remove(buffer, data, sizeof(data) - 1)) != -1)
//   while ((buffer_length = evbuffer_get_length(buffer)) != 0)
  while (true)
  {
    char *skey, *svalue;
    struct evbuffer_ptr delim;
    
    buffer_length = evbuffer_get_length(buffer);

//     req->headers_size += line_length;
// 
//     if (req->evcon != NULL &&
//         req->headers_size > req->evcon->max_headers_size) {
//       errcode = DATA_TOO_LONG;
//       goto error;
//     }
// 
//     if (*line == '\0') { /* Last header - Done */
//       status = ALL_DATA_READ;
//       mm_free(line);
//       break;
//     }
// 
//     /* Check if this is a continuation line */
//     if (*line == ' ' || *line == '\t') {
//       if (evhttp_append_to_last_header(headers, line) == -1)
//         goto error;
//       mm_free(line);
//       continue;
//     }
// 
//     /* Processing of header lines */
//     svalue = line;
//     skey = strsep(&svalue, ":");
//     if (svalue == NULL)
//       goto error;
// 
//     svalue += strspn(svalue, " ");
// 
//     if (evhttp_add_header(headers, skey, svalue) == -1)
//       goto error;
// 
//     mm_free(line);
    
    /* Read in and parse the netstring length declaration if not already done. */
    if (req->headers_declared_size == 0)
    {
      delim = evbuffer_search(buffer, ":", 1, NULL);
      if (delim.pos != -1)
      {
        /* We found the end of netstring size declaration; convert it to size_t */
        size_t decimal_size_len;
        char *decimal_size;
        evbuffer_remove_buffer(buffer, req->input_buffer, delim.pos + 1);
        decimal_size_len = evbuffer_get_length(req->input_buffer) - 1;
        decimal_size = malloc(decimal_size_len + 1);
        evbuffer_remove(req->input_buffer, decimal_size, decimal_size_len + 1);
        decimal_size[decimal_size_len] = '\0'; /* Overwrite the ':' with '\0' */
        /* Validate and convert the "decimal_size" string to integer */
        req->headers_declared_size = validate_decimal_string(decimal_size, decimal_size_len);
        free(decimal_size);
        if (errno != 0 || req->headers_declared_size == 0)
          /* Invalid decimal number or too-small size declaration; return the default DATA_CORRUPTED error. */
          goto error;
        if (req->evcon != NULL && req->headers_declared_size > req->evcon->max_headers_size)
        {
          /* The declared header size is too long; return the DATA_LOO_LONG error. */
          errcode = DATA_TOO_LONG;
          goto error;
        }
        /* Now we're ready to begin reading the headers. */
        req->getting_hdr_value = false; /* We begin by reading a header key. */
        req->have_body_declared_size = false;
        req->found_scgi_hdr = false;
        // TODO: maybe move these initializations to evscgi_request_new? (Though, it does use calloc to allocate the memory, which means that all members are initialized to false / 0 / NULL.)
      }
      else
      {
        /* We only got part of the netstring size declaration; need to wait for more */
        evbuffer_add_buffer(req->input_buffer, buffer);
        status = MORE_DATA_EXPECTED;
        break;
      }
      
      continue;
    }
    
    /* If there are more bytes of the header to read in, then read in the next piece of a header and store the found header once we have both a key and value */
    if (req->headers_size < req->headers_declared_size)
    {
      delim = evbuffer_search(buffer, "\0", 1, NULL);
      if (delim.pos != -1)
      {
        /* We must verify that the header contents found so far are not longer than their declared size */
        if (delim.pos + 1 > req->headers_declared_size - req->headers_size)
          /* The headers are longer than declared; return the default DATA_CORRUPTED error. */
          goto error;
        evbuffer_remove_buffer(buffer, req->input_buffer, delim.pos + 1);
        req->headers_size += delim.pos + 1;
        if (req->getting_hdr_value)
        {
          /* We now have one header in req->input_buffer; must move it to req->input_headers */
          size_t value_len;
          bool parse_error = false;
          int retval;
          delim = evbuffer_search(req->input_buffer, "\0", 1, NULL);
          skey = malloc(delim.pos + 1);
          evbuffer_remove(req->input_buffer, skey, delim.pos + 1);
          value_len = evbuffer_get_length(req->input_buffer) - 1;
          svalue = malloc(value_len + 1);
          evbuffer_remove(req->input_buffer, svalue, value_len + 1);
          /* Verify that first header is CONTENT_LENGTH */
          if (!req->have_body_declared_size)
          {
            if (strcmp(skey, "CONTENT_LENGTH") == 0)
            {
              /* Validate and convert the CONTENT_LENGTH value. */
              req->body_declared_size = validate_decimal_string(svalue, value_len);
              if (errno != 0)
              {
                /* Invalid decimal number; return the default DATA_CORRUPTED error. */
                free(skey);
                free(svalue);
                goto error;
              }
              req->have_body_declared_size = true;
              if (req->body_declared_size > req->evcon->max_body_size)
              {
                /* The declared body size is too long; return the DATA_LOO_LONG error. */
                free(skey);
                free(svalue);
                errcode = DATA_TOO_LONG;
                goto error;
              }
            }
            else
            {
              /* First header was not CONTENT_LENGTH as required by SCGI spec; return the default DATA_CORRUPTED error. */
              free(skey);
              free(svalue);
              goto error;
            }
          }
          else
          {
            // For all specific headers we want to parse except CONTENT_LENGTH, we have a hash-dictionary where keys are the header names and values are pointers to parser functions.
            PWord_t dict_value_ptr;
            JSLG(dict_value_ptr, req->evcon->scgi_server->hdrparsers_by_name, skey);
            if (dict_value_ptr != NULL)
              parse_error = !(hdrparser[*dict_value_ptr](req, svalue));
          }
          if (!parse_error)
          {
            if (req->header_cb != NULL)
              (*req->header_cb)(req, req->header_cb_arg, skey, svalue);
            else
              retval = evhttp_add_header(headers, skey, svalue);
          }
          free(skey);
          free(svalue);
          if (parse_error || retval == -1)
            goto error;
          /* If there are more headers to read, then the next part we must read is a header key. */
          req->getting_hdr_value = false;
        }
        else
          /* We now have half of a header (a header key) in req->input_buffer; need to read its corresponding value next. */
          req->getting_hdr_value = true;
      }
      else
      {
        /* We must verify that the header contents found so far are not longer than their declared size */
        if (buffer_length > req->headers_declared_size - req->headers_size)
          /* Either the headers are longer than declared, or the header data doesn't end with a '\0'; in either case, the request is corrupt, so we return the default DATA_CORRUPTED error. */
          goto error;
        /* We only got part of the header; need to wait for more */
        evbuffer_add_buffer(req->input_buffer, buffer);
        req->headers_size += buffer_length;
        status = MORE_DATA_EXPECTED;
        break;
      }
      
      continue;
    }
    
    /* Do this part when we've finished reading in the declared length of header data. */
    if (req->headers_size == req->headers_declared_size)
    {
      if (req->headersdone_cb != NULL)
        (*req->headersdone_cb)(req, req->header_cb_arg);
      
      /* The header data must be followed by a ',', or else the request is invalid. */
      if (buffer_length > 0)
      {
        /* At least one byte remains in buffer, so read the first one and verify that it's a ','. */
        char end_marker;
        evbuffer_remove(buffer, &end_marker, 1);
        if (end_marker == ',')
        {
          /* We've validated the headers. Any remaining bytes of input are part of the request body, so we need to move them to req->input_buffer. */
          if (buffer_length > 1)
            evbuffer_add_buffer(req->input_buffer, buffer);
          
          status = ALL_DATA_READ;
          break;
        }
        else
          /* The headers are longer than declared; return the default DATA_CORRUPTED error. */
          goto error;
      }
      else
      {
        /* We haven't yet received the first byte after the headers; need to wait for it. */
        status = MORE_DATA_EXPECTED;
        break;
      }
      
      // No need to do "continue;" here, because we can't actually get here.
    }
    
    /* If we've emptied "buffer" without reaching the end of header data, we must return MORE_DATA_EXPECTED status. */
    if (buffer_length == 0)
    {
      status = MORE_DATA_EXPECTED;
      break;
    }
  }

//   if (status == MORE_DATA_EXPECTED) {
//     if (req->evcon != NULL &&
//     req->headers_size + evbuffer_get_length(buffer) > req->evcon->max_headers_size)
//       return (DATA_TOO_LONG);
//   }

  /* We're returning a non-error status (either MORE_DATA_EXPECTED or ALL_DATA_READ); this requires that "buffer" has been completely emptied by this point. */
  return (status);

 error:
//   mm_free(line);
  return (errcode);
}

static size_t validate_decimal_string(const char *decimal, size_t decimal_len)
{
  size_t retval;
  errno = 0; /* Caller must check errno before checking the return value. */
  /* Verify that "decimal" string contains only decimal digits */
  if (strspn(decimal, "0123456789") != decimal_len)
  {
    /* Invalid decimal number; indicate error. */
    errno = EINVAL;
    return 0;
  }
  retval = strtoul(decimal, NULL, 10);
  /* Don't allow leading zeroes in non-zero number. (Or should we allow them?) */
  if (errno == 0 && retval != 0 && decimal[0] == '0')
  {
    errno = EINVAL;
    return 0;
  }
  return retval;
}

static void evscgi_get_body(struct evscgi_connection *evcon, struct evscgi_request *req)
{
  /* Based on evhttp_get_body */

  /* If this is a request without a body, then we are done */
  if (req->kind == EVSCGI_REQUEST)
  {
    if (!req->have_body_declared_size)
    {
      /* TODO: Check if this case is actually reachable. (Maybe we would have already failed the connection before we get here in case of missing CONTENT_LENGTH header?) */
      evscgi_connection_fail(evcon, EVCON_SCGI_INVALID_HEADER);
      return;
    }
    else if (req->body_declared_size == 0)
    {
      evscgi_connection_done(evcon);
      return;
    }
  }
  
  evcon->state = EVCON_READING_BODY;
  /* SCGI request body can't be in chunked transfer-encoding; SCGI response might be, though. */
  /* However, we just pass the response body through unchanged, so we don't need to do any special handling for chunked encoding. */
  
  /* I doubt that CGI (and therefore SCGI) request handling needs any special behavior for the "HTTP_EXPECT" header, so remove this next part. (HTTP "Expect:" header is only treated specially by the HTTP server, I suppose.) */
//   /* Should we send a 100 Continue status line? */
//   if (req->kind == EVHTTP_REQUEST && REQ_VERSION_ATLEAST(req, 1, 1)) {
//     const char *expect;
// 
//     expect = evhttp_find_header(req->input_headers, "Expect");
//     if (expect) {
//       if (!evutil_ascii_strcasecmp(expect, "100-continue")) {
//         /* XXX It would be nice to do some sanity
//            checking here. Does the resource exist?
//            Should the resource accept post requests? If
//            no, we should respond with an error. For
//            now, just optimistically tell the client to
//            send their message body. */
//         if (req->ntoread > 0) {
//           /* ntoread is ev_int64_t, max_body_size is ev_uint64_t */ 
//           if ((req->evcon->max_body_size <= EV_INT64_MAX) && (ev_uint64_t)req->ntoread > req->evcon->max_body_size) {
//             evhttp_send_error(req, HTTP_ENTITYTOOLARGE, NULL);
//             return;
//           }
//         }
//         if (!evbuffer_get_length(bufferevent_get_input(evcon->bufev)))
//           evhttp_send_continue(evcon, req);
//       } else {
//         evhttp_send_error(req, HTTP_EXPECTATIONFAILED,
//           NULL);
//         return;
//       }
//     }
//   }

  evscgi_read_body(evcon, req);
  /* note the request may have been freed in evscgi_read_body */
}

static void evscgi_read_body(struct evscgi_connection *evcon, struct evscgi_request *req)
{
  /* Based on evhttp_read_body */
  struct evbuffer *buf = bufferevent_get_input(evcon->bufev);
  size_t buffer_length = evbuffer_get_length(buf);
  
//   if (req->kind == EVSCGI_REQUEST)
//   {
  // NOTE: req->kind must always be EVSCGI_REQUEST here, because we're not implementing the client side of SCGI (and therefore we will never read the body of a response).

  /* NOTE: In SCGI, the request body cannot be in chunked encoding (but the response body can). */
  
  /* We've postponed moving the data until now, but we're about to use it. */
  ev_uint64_t tentative_size = req->body_size;
  tentative_size += buffer_length;
  if (tentative_size > req->body_declared_size
      || tentative_size < buffer_length /* An overflow condition */
      || tentative_size < req->body_size) /* An overflow condition */
  {
    /* The actual size of request body is longer than declared. (NOTE: We already checked that declared size was less than the maximum allowed size, so don't need to recheck it here.) */
    evscgi_connection_fail(evcon, EVCON_SCGI_INVALID_HEADER);
    return;
  }
  evbuffer_remove_buffer(buf, req->input_buffer, buffer_length);
  req->body_size = tentative_size;
  
  if (evbuffer_get_length(req->input_buffer) > 0 && req->chunk_cb != NULL)
  {
    req->flags |= EVSCGI_REQ_DEFER_FREE;
    (*req->chunk_cb)(req, req->chunk_cb_arg);
    req->flags &= ~EVSCGI_REQ_DEFER_FREE;
    evbuffer_drain(req->input_buffer, evbuffer_get_length(req->input_buffer));
    if ((req->flags & EVSCGI_REQ_NEEDS_FREE) != 0)
    {
      evscgi_request_free(req);
      return;
    }
  }
  
  if (req->body_size == req->body_declared_size)
  {
    if (req->chunksdone_cb != NULL)
      (*req->chunksdone_cb)(req, req->chunk_cb_arg);
    
    bufferevent_disable(evcon->bufev, EV_READ);
    /* Completed content length */
    evscgi_connection_done(evcon);
    return;
  }
//   }
//   else // req->kind == EVSCGI_RESPONSE
//   {
//   }
  
  /* Read more! */
  bufferevent_enable(evcon->bufev, EV_READ);
}

static inline void evscgi_send(struct evscgi_request *req, struct evbuffer *databuf)
{
  /* Based on evhttp_send */
  /* Requires that headers and response code are already set up */

  struct evscgi_connection *evcon = req->evcon;

  if (evcon == NULL) {
    evscgi_request_free(req);
    return;
  }

  assert(TAILQ_FIRST(&evcon->requests) == req);

  /* we expect no more calls from the user on this request */
  req->userdone = 1;

  /* xxx: not sure if we really should expose the data buffer this way */
  if (databuf != NULL)
    evbuffer_add_buffer(req->output_buffer, databuf);

  /* Adds headers to the response */
  evscgi_make_header(evcon, req);

  evscgi_write_buffer(evcon, evscgi_send_done, NULL);
}

static void evscgi_send_done(struct evscgi_connection *evcon, void *arg)
{
  /* Based on evhttp_send_done */
//   int need_close;
  struct evscgi_request *req = TAILQ_FIRST(&evcon->requests);
  TAILQ_REMOVE(&evcon->requests, req, next);

//   need_close =
//       (REQ_VERSION_BEFORE(req, 1, 1) &&
//     !evhttp_is_connection_keepalive(req->input_headers))||
//       evhttp_is_connection_close(req->flags, req->input_headers) ||
//       evhttp_is_connection_close(req->flags, req->output_headers);
// 
//   EVUTIL_ASSERT(req->flags & EVHTTP_REQ_OWN_CONNECTION);
//   evhttp_request_free(req);
// 
//   if (need_close) {
//     evhttp_connection_free(evcon);
//     return;
//   }
// 
//   /* we have a persistent connection; try to accept another request. */
//   if (evhttp_associate_new_request_with_connection(evcon) == -1) {
//     evhttp_connection_free(evcon);
//   }
  
  assert(req->flags & EVSCGI_REQ_OWN_CONNECTION);
  evscgi_request_free(req);

  /* Unlike HTTP/1.1, SCGI _always_ requires closing the connection when the response has been completely sent. */
  evscgi_connection_free(evcon);
}

static void evscgi_write_buffer(struct evscgi_connection *evcon, void (*cb)(struct evscgi_connection *, void *), void *arg)
{
  // Based on evhttp_write_buffer
//   event_debug(("%s: preparing to write buffer\n", __func__));

  /* Set call back */
  evcon->cb = cb;
  evcon->cb_arg = arg;

  bufferevent_enable(evcon->bufev, EV_WRITE);

  /* Disable the read callback: we don't actually care about data;
   * we only care about close detection.  (We don't disable reading,
   * since we *do* want to learn about any close events.) */
  bufferevent_setcb(evcon->bufev,
      NULL, /*read*/
      evscgi_write_cb,
      evscgi_error_cb,
      evcon);
}

/** Generate all headers appropriate for sending the http request in req (or
 * the response, if we're sending a response), and write them to evcon's
 * bufferevent. Also writes all data from req->output_buffer */
static void evscgi_make_header(struct evscgi_connection *evcon, struct evscgi_request *req)
{
  // Based on evhttp_make_header
  struct evkeyval *header;
  struct evbuffer *output = bufferevent_get_output(evcon->bufev);

//   /*
//    * Depending if this is a HTTP request or response, we might need to
//    * add some new headers or remove existing headers.
//    */
//   if (req->kind == EVHTTP_REQUEST) {
//     evhttp_make_header_request(evcon, req);
//   } else {
//     evhttp_make_header_response(evcon, req);
//   }
  /* This can only be an SCGI response, since we're not implementing the client side of SCGI, at least not yet. */
  evscgi_make_header_response(evcon, req);

  TAILQ_FOREACH(header, req->output_headers, next) {
    evbuffer_add_printf(output, "%s: %s\r\n",
        header->key, header->value);
  }
  evbuffer_add(output, "\r\n", 2);

  if (evbuffer_get_length(req->output_buffer) > 0) {
    /*
     * For a request, we add the POST data, for a reply, this
     * is the regular data.
     */
    /* XXX We might want to support waiting (a limited amount of
       time) for a continue status line from the server before
       sending POST/PUT message bodies. */
    evbuffer_add_buffer(output, req->output_buffer);
  }
}

/*
 * Create the headers needed for an SCGI response in req->output_headers,
 * and write the first SCGI response for req line to evcon.
 */
static void evscgi_make_header_response(struct evscgi_connection *evcon, struct evscgi_request *req)
{
  // Based on evhttp_make_header_response
//   int is_keepalive = evhttp_is_connection_keepalive(req->input_headers);
//   evbuffer_add_printf(bufferevent_get_output(evcon->bufev),
//       "HTTP/%d.%d %d %s\r\n",
//       req->major, req->minor, req->response_code,
//       req->response_code_line);
  evbuffer_add_printf(bufferevent_get_output(evcon->bufev), "Status: %d %s\r\n",
                      req->response_code, req->response_code_line);
//   if (req->major == 1) {
//     if (req->minor >= 1)
//       evhttp_maybe_add_date_header(req->output_headers);
// 
//     /*
//      * if the protocol is 1.0; and the connection was keep-alive
//      * we need to add a keep-alive header, too.
//      */
//     if (req->minor == 0 && is_keepalive)
//       evhttp_add_header(req->output_headers,
//           "Connection", "keep-alive");
// 
//     if ((req->minor >= 1 || is_keepalive) &&
//         evhttp_response_needs_body(req)) {
//       /*
//        * we need to add the content length if the
//        * user did not give it, this is required for
//        * persistent connections to work.
//        */
//       evhttp_maybe_add_content_length_header(
//         req->output_headers,
//         evbuffer_get_length(req->output_buffer));
//     }
//   }
  evscgi_maybe_add_content_length_header(req->output_headers, evbuffer_get_length(req->output_buffer));

  /* Potentially add headers for unidentified content. */
  if (evscgi_response_needs_body(req)) {
    if (evhttp_find_header(req->output_headers,
      "Content-Type") == NULL) {
      evhttp_add_header(req->output_headers,
          "Content-Type", "text/html; charset=UTF-8");
    }
  }

//   /* if the request asked for a close, we send a close, too */
//   if (evhttp_is_connection_close(req->flags, req->input_headers)) {
//     evhttp_remove_header(req->output_headers, "Connection");
//     if (!(req->flags & EVHTTP_PROXY_REQUEST))
//         evhttp_add_header(req->output_headers, "Connection", "close");
//     evhttp_remove_header(req->output_headers, "Proxy-Connection");
//   }
}

/* Add a "Content-Length" header with value 'content_length' to headers,
 * unless it already has a content-length or transfer-encoding header. */
static void evscgi_maybe_add_content_length_header(struct evkeyvalq *headers, size_t content_length)
{
  // Based on evhttp_maybe_add_content_length_header
  if (evhttp_find_header(headers, "Transfer-Encoding") == NULL &&
      evhttp_find_header(headers, "Content-Length") == NULL) {
    char len[22];
    evutil_snprintf(len, sizeof(len), "%zu", content_length);
    evhttp_add_header(headers, "Content-Length", len);
  }
}

/**
 * Determines if a response should have a body.
 * Follows the rules in RFC 2616 section 4.3.
 * @return 1 if the response MUST have a body; 0 if the response MUST NOT have
 *     a body.
 */
static int evscgi_response_needs_body(struct evscgi_request *req)
{
  // Based on evhttp_response_needs_body
  return (req->response_code != HTTP_NOCONTENT &&
    req->response_code != HTTP_NOTMODIFIED &&
    (req->response_code < 100 || req->response_code >= 200) &&
    req->type != EVSCGI_REQ_HEAD);
}

// Response status code phrase lookup tables & function originally from http.c:
static const char *informational_phrases[] = {
  /* 100 */ "Continue",
  /* 101 */ "Switching Protocols"
};

static const char *success_phrases[] = {
  /* 200 */ "OK",
  /* 201 */ "Created",
  /* 202 */ "Accepted",
  /* 203 */ "Non-Authoritative Information",
  /* 204 */ "No Content",
  /* 205 */ "Reset Content",
  /* 206 */ "Partial Content"
};

static const char *redirection_phrases[] = {
  /* 300 */ "Multiple Choices",
  /* 301 */ "Moved Permanently",
  /* 302 */ "Found",
  /* 303 */ "See Other",
  /* 304 */ "Not Modified",
  /* 305 */ "Use Proxy",
  /* 307 */ "Temporary Redirect"
};

static const char *client_error_phrases[] = {
  /* 400 */ "Bad Request",
  /* 401 */ "Unauthorized",
  /* 402 */ "Payment Required",
  /* 403 */ "Forbidden",
  /* 404 */ "Not Found",
  /* 405 */ "Method Not Allowed",
  /* 406 */ "Not Acceptable",
  /* 407 */ "Proxy Authentication Required",
  /* 408 */ "Request Time-out",
  /* 409 */ "Conflict",
  /* 410 */ "Gone",
  /* 411 */ "Length Required",
  /* 412 */ "Precondition Failed",
  /* 413 */ "Request Entity Too Large",
  /* 414 */ "Request-URI Too Large",
  /* 415 */ "Unsupported Media Type",
  /* 416 */ "Requested range not satisfiable",
  /* 417 */ "Expectation Failed"
};

static const char *server_error_phrases[] = {
  /* 500 */ "Internal Server Error",
  /* 501 */ "Not Implemented",
  /* 502 */ "Bad Gateway",
  /* 503 */ "Service Unavailable",
  /* 504 */ "Gateway Time-out",
  /* 505 */ "HTTP Version not supported"
};

struct response_class {
  const char *name;
  size_t num_responses;
  const char **responses;
};

#ifndef MEMBERSOF
#define MEMBERSOF(x) (sizeof(x)/sizeof(x[0]))
#endif

static const struct response_class response_classes[] = {
  /* 1xx */ { "Informational", MEMBERSOF(informational_phrases), informational_phrases },
  /* 2xx */ { "Success", MEMBERSOF(success_phrases), success_phrases },
  /* 3xx */ { "Redirection", MEMBERSOF(redirection_phrases), redirection_phrases },
  /* 4xx */ { "Client Error", MEMBERSOF(client_error_phrases), client_error_phrases },
  /* 5xx */ { "Server Error", MEMBERSOF(server_error_phrases), server_error_phrases }
};

static const char *evscgi_response_phrase_internal(int code)
{
  // Based on evhttp_response_phrase_internal
  int klass = code / 100 - 1;
  int subcode = code % 100;

  /* Unknown class - can't do any better here */
  if (klass < 0 || klass >= (int) MEMBERSOF(response_classes))
    return "Unknown Status Class";

  /* Unknown sub-code, return class name at least */
  if (subcode >= (int) response_classes[klass].num_responses)
    return response_classes[klass].name;

  return response_classes[klass].responses[subcode];
}

// Internal-only header manipulation functions:
// static int evscgi_header_is_valid_value(const char *value)
// {
//   // Based on evhttp_header_is_valid_value
//   const char *p = value;
// 
//   while ((p = strpbrk(p, "\r\n")) != NULL) {
//     /* we really expect only one new line */
//     p += strspn(p, "\r\n");
//     /* we expect a space or tab for continuation */
//     if (*p != ' ' && *p != '\t')
//       return (0);
//   }
//   return (1);
// }
// 
// static int evscgi_add_header_internal(struct evkeyvalq *headers, const char *key, const char *value)
// {
//   struct evkeyval *header = calloc(1, sizeof(struct evkeyval));
//   if (header == NULL) {
// //     event_warn("%s: calloc", __func__);
//     return (-1);
//   }
//   if ((header->key = strdup(key)) == NULL) {
//     free(header);
// //     event_warn("%s: strdup", __func__);
//     return (-1);
//   }
//   if ((header->value = strdup(value)) == NULL) {
//     free(header->key);
//     free(header);
// //     event_warn("%s: strdup", __func__);
//     return (-1);
//   }
// 
//   TAILQ_INSERT_TAIL(headers, header, next);
// 
//   return (0);
// }

static void evscgi_send_page(struct evscgi_request *req, struct evbuffer *databuf)
{
  // Based on evhttp_send_page
//   if (!req->major || !req->minor) { // NOTE: Looks like a bug to me -- shouldn't it only assign a version in case *both* major and minor are zero? (They're initialized to zero by calloc in evscgi_connection_base_new, and later have values assigned upon parsing the SERVER_PROTOCOL header if it's present.)
  if (!req->major && !req->minor) {
    req->major = 1;
    req->minor = 1;
  }

  if (req->kind != EVSCGI_RESPONSE)
    evscgi_response_code(req, 200, "OK");

  evhttp_clear_headers(req->output_headers);
  evhttp_add_header(req->output_headers, "Content-Type", "text/html");
  evhttp_add_header(req->output_headers, "Connection", "close");

  evscgi_send(req, databuf);
}

// Header-parser functions here:

static bool parsehdr_SCGI(struct evscgi_request *req, const char *value)
{
  if (!req->found_scgi_hdr)
  {
    if (strcmp(value, "1") == 0)
      req->found_scgi_hdr = true;
    else
      return false;
  }
  return true;
}

static bool parsehdr_REQUEST_URI(struct evscgi_request *req, const char *value)
{
  // Based on evhttp_parse_request_line
  if (req->uri == NULL)
  {
    if ((req->uri = strdup(value)) == NULL)
//       event_debug(("%s: mm_strdup", __func__));
      return false;
    
    if ((req->uri_elems = evhttp_uri_parse_with_flags(req->uri,
          EVHTTP_URI_NONCONFORMANT)) == NULL)
      return false;
  }
  return true;
}

#define _EVSCGI_REQ_UNKNOWN (1<<15)

static bool parsehdr_REQUEST_METHOD(struct evscgi_request *req, const char *value)
{
  // Partly based on evhttp_parse_request_line
  if (!req->type)
  {
    PWord_t dict_value_ptr;
    JSLG(dict_value_ptr, req->evcon->scgi_server->methods_by_name, value);
    if (dict_value_ptr != NULL)
      req->type = *dict_value_ptr;
    else
//       event_debug(("%s: bad method %s on request %p from %s",
//         __func__, method, req, req->remote_host));
      req->type = _EVSCGI_REQ_UNKNOWN;
      /* No error yet; we'll give a better error later when
       * we see that req->type is unsupported. */
  }
  return true;
}

static bool parsehdr_SERVER_PROTOCOL(struct evscgi_request *req, const char *value)
{
  // Based on evhttp_parse_http_version
  if (!req->major && !req->minor)
  {
    int major, minor;
    // NOTE: Do we really need the %c part of the scan-format? Let's try it without.
//     char ch;
//     int n = sscanf(svalue, "HTTP/%d.%d%c", &major, &minor, &ch);
    int n = sscanf(value, "HTTP/%d.%d", &major, &minor);
    if (n > 2 || major > 1)
//       event_debug(("%s: bad version %s on message %p from %s",
//         __func__, version, req, req->remote_host));
      return false;
    
    req->major = major;
    req->minor = minor;
  }
  return true;
}

#ifdef __cplusplus
}
#endif
