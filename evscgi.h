/*
 * Copyright (c) 2002-2007 Niels Provos <provos@citi.umich.edu>
 * Copyright (c) 2007-2011 Niels Provos and Nick Mathewson
 * Copyright (c) 2012 Andrew Munkres <amunkres@nyx.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _EVSCGI_H_
#define _EVSCGI_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

/* For evkeyvalq */
#include <event2/event_struct.h>

/* For TAILQ_ENTRY */
#include <sys/queue.h>

/* For integer typedefs */
#include <event2/util.h>

/* Based on evhttp_cmd_type */
enum evscgi_cmd_type {
  EVSCGI_REQ_GET     = 1 << 0,
  EVSCGI_REQ_POST    = 1 << 1,
  EVSCGI_REQ_HEAD    = 1 << 2,
  EVSCGI_REQ_PUT     = 1 << 3,
  EVSCGI_REQ_DELETE  = 1 << 4,
  EVSCGI_REQ_OPTIONS = 1 << 5,
  EVSCGI_REQ_TRACE   = 1 << 6,
  EVSCGI_REQ_CONNECT = 1 << 7,
  EVSCGI_REQ_PATCH   = 1 << 8
};

enum evscgi_notify_mode { EVSCGI_NOTIFY_NEW_CONNECTION, EVSCGI_NOTIFY_RESPONSE_NEEDED };

struct evscgi;
struct evscgi_connection;
struct evscgi_request;
struct evconnlistener;

int evscgi_accept_socket(struct evscgi *scgi, evutil_socket_t fd);
struct evscgi_bound_socket *evscgi_accept_socket_with_handle(struct evscgi *scgi, evutil_socket_t fd);
struct evscgi_bound_socket *evscgi_bind_listener(struct evscgi *scgi, struct evconnlistener *listener);
int evscgi_bind_unix_socket(struct evscgi *scgi, const char *pathname);
struct evscgi_bound_socket *evscgi_bind_unix_socket_with_handle(struct evscgi *scgi, const char *pathname);
evutil_socket_t evscgi_bound_socket_get_fd(struct evscgi_bound_socket *bound);
struct evconnlistener *evscgi_bound_socket_get_listener(struct evscgi_bound_socket *bound);
void evscgi_cancel_request(struct evscgi_request *req);
struct evscgi_connection *evscgi_unix_connection_base_new(struct event_base *base, const char *pathname);
void evscgi_connection_free(struct evscgi_connection *evcon);
struct event_base *evscgi_connection_get_base(struct evscgi_connection *req);
void evscgi_connection_set_closecb(struct evscgi_connection *evcon, void(*)(struct evscgi_connection *, void *), void *);
void evscgi_connection_set_max_body_size(struct evscgi_connection *evcon, ev_ssize_t new_max_body_size);
void evscgi_connection_set_max_headers_size(struct evscgi_connection *evcon, ev_ssize_t new_max_headers_size);
void evscgi_connection_set_timeout(struct evscgi_connection *evcon, int timeout_in_secs);
void evscgi_del_accept_socket(struct evscgi *scgi, struct evscgi_bound_socket *bound_socket);
int evscgi_del_cb(struct evscgi *scgi, const char *uri);
void evscgi_free(struct evscgi* scgi);
struct evscgi *evscgi_new(struct event_base *base);

// These are "getter" functions for evscgi_request:
enum evscgi_cmd_type evscgi_request_get_command(const struct evscgi_request *req);
struct evscgi_connection *evscgi_request_get_connection (struct evscgi_request *req);
const struct evhttp_uri *evscgi_request_get_evhttp_uri (const struct evscgi_request *req);
const char *evscgi_request_get_host (struct evscgi_request *req);
struct evbuffer *evscgi_request_get_input_buffer (struct evscgi_request *req);
struct evkeyvalq *evscgi_request_get_input_headers (struct evscgi_request *req);
struct evbuffer *evscgi_request_get_output_buffer (struct evscgi_request *req);
struct evkeyvalq *evscgi_request_get_output_headers (struct evscgi_request *req);
int evscgi_request_get_response_code(const struct evscgi_request *req);
const char *evscgi_request_get_uri(const struct evscgi_request *req);

void evscgi_request_own(struct evscgi_request *req);
void evscgi_request_set_chunked_cbs(struct evscgi_request *req, void (*chunk_cb)(struct evscgi_request *, void *), void (*chunksdone_cb)(struct evscgi_request *, void *), void *chunk_cb_arg);
void evscgi_request_set_header_cb(struct evscgi_request *req, void (*header_cb)(struct evscgi_request *, void *, const char *, const char *), void (*headersdone_cb)(struct evscgi_request *, void *), void *header_cb_arg);
void evscgi_send_error(struct evscgi_request *req, int error, const char *reason);
void evscgi_send_reply(struct evscgi_request *req, int code, const char *reason, struct evbuffer *databuf);
void evscgi_send_reply_chunk(struct evscgi_request *req, struct evbuffer *databuf);
void evscgi_send_reply_end(struct evscgi_request *req);
void evscgi_send_reply_start(struct evscgi_request *req, int code, const char *reason);
void evscgi_set_allowed_methods(struct evscgi *scgi, ev_uint16_t methods);
int evscgi_set_cb(struct evscgi *scgi, const char *path, void(*cb)(struct evscgi_request *, void *), void *cbarg);
void evscgi_set_gencb(struct evscgi *scgi, void(*cb)(struct evscgi_request *, void *), void *cbarg);
void evscgi_set_max_body_size(struct evscgi *scgi, ev_ssize_t max_body_size);
void evscgi_set_max_headers_size(struct evscgi *scgi, ev_ssize_t max_headers_size);
void evscgi_set_notify_on(struct evscgi *scgi, enum evscgi_notify_mode when);
void evscgi_set_timeout(struct evscgi* scgi, int timeout_in_secs);

// The original evhttp header-handling functions are:
// int evhttp_add_header(struct evkeyvalq *headers, const char *key, const char *value);
// void evhttp_clear_headers(struct evkeyvalq *headers);
// const char *evhttp_find_header(const struct evkeyvalq *headers, const char *key);
// int evhttp_remove_header(struct evkeyvalq *headers, const char *key);
// NOTE: Currently we use these internally, but we want an API for the user to receive and send headers without needing access to the struct data members. We also want this API to allow piecemeal send/receive of headers (even if we don't yet implement it that way). The evscgi_request_set_header_cb method is what we use for piecemeal receiving of request headers, and here is what we will use to send response headers:
void evscgi_send_header(struct evscgi_request *req, const char *key, const char *value);

// NOTE: The following URI and HTML handling functions from evhttp can be used unmodified in SCGI, so we don't need to port them:
// char *evhttp_decode_uri(const char *uri);
// char *evhttp_encode_uri(const char *str);
// char *evhttp_htmlescape(const char *html);
// int evhttp_parse_query(const char *uri, struct evkeyvalq *headers);
// int evhttp_parse_query_str(const char *uri, struct evkeyvalq *headers);
// void evhttp_uri_free (struct evhttp_uri *uri);
// const char *evhttp_uri_get_fragment (const struct evhttp_uri *uri);
// const char *evhttp_uri_get_host (const struct evhttp_uri *uri);
// const char *evhttp_uri_get_path (const struct evhttp_uri *uri);
// int evhttp_uri_get_port (const struct evhttp_uri *uri);
// const char *evhttp_uri_get_query (const struct evhttp_uri *uri);
// const char *evhttp_uri_get_scheme (const struct evhttp_uri *uri);
// const char *evhttp_uri_get_userinfo (const struct evhttp_uri *uri);
// char *evhttp_uri_join (struct evhttp_uri *uri, char *buf, size_t limit);
// struct evhttp_uri *evhttp_uri_new (void);
// struct evhttp_uri *evhttp_uri_parse (const char *source_uri);
// struct evhttp_uri *evhttp_uri_parse_with_flags (const char *source_uri, unsigned flags);
// void evhttp_uri_set_flags (struct evhttp_uri *uri, unsigned flags);
// int evhttp_uri_set_fragment (struct evhttp_uri *uri, const char *fragment);
// int evhttp_uri_set_host (struct evhttp_uri *uri, const char *host);
// int evhttp_uri_set_path (struct evhttp_uri *uri, const char *path);
// int evhttp_uri_set_port (struct evhttp_uri *uri, int port);
// int evhttp_uri_set_query (struct evhttp_uri *uri, const char *query);
// int evhttp_uri_set_scheme (struct evhttp_uri *uri, const char *scheme);
// int evhttp_uri_set_userinfo (struct evhttp_uri *uri, const char *userinfo);
// char *evhttp_uridecode (const char *uri, int decode_plus, size_t *size_out);
// char *evhttp_uriencode (const char *str, ev_ssize_t size, int space_to_plus);

// NOTE: These virtual host-related functions from evhttp are inapplicable to SCGI:
// int evhttp_add_server_alias(struct evhttp *http, const char *alias);
// int evhttp_add_virtual_host(struct evhttp *http, const char *pattern, struct evhttp *vhost);
// int evhttp_remove_server_alias(struct evhttp *http, const char *alias)
// int evhttp_remove_virtual_host(struct evhttp *http, struct evhttp *vhost)

// NOTE: We're not yet implementing the client side of SCGI, so we don't have these functions:
// int evhttp_make_request(struct evhttp_connection *evcon, struct evhttp_request *req, enum evhttp_cmd_type type, const char *uri);
// void evhttp_connection_set_retries(struct evhttp_connection *evcon, int retry_max);

// NOTE: We're currently only supporting AF_UNIX sockets, so we don't have these functions for handling AF_INET ones:
// int evhttp_bind_socket(struct evhttp *http, const char *address, ev_uint16_t port);
// struct evhttp_bound_socket *evhttp_bind_socket_with_handle(struct evhttp *http, const char *address, ev_uint16_t port);
// void evhttp_connection_get_peer(struct evhttp_connection *evcon, char **address, ev_uint16_t *port);
// void evhttp_connection_set_local_address(struct evhttp_connection *evcon, const char *address);
// void evhttp_connection_set_local_port(struct evhttp_connection *evcon, ev_uint16_t port);
// struct evhttp_connection *evhttp_connection_base_new(struct event_base *base, struct evdns_base *dnsbase, const char *address, unsigned short port);

// Maybe later on port some of these functions from evhttp:
//
// struct bufferevent *evhttp_connection_get_bufferevent(struct evhttp_connection *evcon); NOTE this one is listed in the libevent API docs but doesn't seem to be in the actual code!

#ifdef __cplusplus
}
#endif

#endif /* _EVSCGI_H_ */
