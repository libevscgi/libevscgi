prefix=@CMAKE_INSTALL_PREFIX@
exec_prefix=@CMAKE_INSTALL_PREFIX@
libdir=@LIB_INSTALL_DIR@
includedir=@INCLUDE_INSTALL_DIR@

Name: libevscgi
Description: SCGI server library that uses libevent
Requires: libevent
Libs: -L${libdir} -levscgi
Cflags: -I${includedir}
